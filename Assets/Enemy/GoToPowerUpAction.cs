﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Actions/GoToPowerUp")]
public class GoToPowerUpAction : Action {

    public override void Act(StateController controller)
    {
        TakePowerUp(controller);
    }

    private void TakePowerUp (StateController controller)
    {
        controller.navMeshAgent.ResetPath();
        controller.navMeshAgent.speed = controller.enemyStats.runningSpeed;

        if (!controller.powerUpTarget)
        {
            Debug.Log("Target removed");
        }
        else if (!controller.powerUpTarget.gameObject.activeSelf)
        {
            Debug.Log("Target inactive");
        }
        else
        {
            controller.navMeshAgent.SetDestination(controller.powerUpTarget.position);
            if (controller.Ani != null && controller.Ani.enabled && !controller.Ani.GetCurrentAnimatorStateInfo(0).IsName("RunState"))
            {
                controller.Ani.Play("RunState", 0);
                //controller.Ani.Play()
            }
        }
    }
}
