﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public Transform target;

    private float timer;
    private EnemyHealth enemyHealth;
    private EnemyMovement enemyMovement;
    private EnemyStats enemyStats;

    void Awake()
    {
        enemyHealth = GetComponent<EnemyHealth>();
        enemyMovement = GetComponent<EnemyMovement>();
        enemyStats = GetComponent<EnemyStats>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        foreach (Transform player in enemyMovement.players)
        {
            if (timer >= enemyStats.attackRate &&
                enemyMovement.currentState == EnemyMovement.EnemyState.FIGHTING &&
                enemyHealth.health > 0)
            {
                target = player;
                Attack(target);
            }
        }
    }

    void Attack(Transform t)
    {
        timer = 0f;
        int dmg = enemyStats.attackDamage;

        if (t.GetComponent<PlayerHealth>().health > 0)
        {
            t.GetComponent<PlayerHealth>().TakeDamage(dmg);
            Debug.Log("Player took " + dmg + " damage.");
        }
    }
}
