﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayer : MonoBehaviour
{

    public float speed = 10f;

    Vector3 movement;
    Rigidbody playerRB;

    void Awake()
    {
        playerRB = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        float hor = Input.GetAxisRaw("Horizontal");
        float ver = Input.GetAxisRaw("Vertical");

        Move(hor, ver);
    }

    void Move(float hor, float ver)
    {
        movement.Set(hor, 0f, ver);

        movement = movement.normalized * speed * Time.deltaTime;

        playerRB.MovePosition(transform.position + movement);
    }


}
