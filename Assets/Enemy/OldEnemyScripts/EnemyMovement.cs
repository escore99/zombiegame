﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private GameObject[] escapePoints;
    private GameObject currentEscapePoint;
    private int randomIndex;

    private Transform foodStorage;
    private Transform enemyPowerUp;
    private Transform commonPowerUp;

    private float runningDistance = 30f;
    private float fightDistance = 15f;
    private float powerUpDistance = 20f;

    private float searchingSpeed = 2f;
    private float runningSpeed = 8f;

    private float distanceToPlayer;
    private float distanceToCommonPowerUp;
    private float distanceToEnemyPowerUp;

    [HideInInspector]
    public NavMeshAgent nav;

    [HideInInspector]
    public Transform player;

    [HideInInspector]
    public List<Transform> players = new List<Transform>();

    bool canLookForPlayers = true;
    bool canLookForPowerUps = true;

    public enum EnemyState
    {
        SEARCHING,
        RUNNING,
        FIGHTING,
        FOUNDCOMMONPOWERUP,
        FOUNDENEMYPOWERUP
    }

    public EnemyState currentState;

    void Awake()
    {
        foodStorage = GameObject.FindGameObjectWithTag("FoodStorage").transform;
        escapePoints = GameObject.FindGameObjectsWithTag("EscapePoint");
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //commonPowerUp = GameObject.FindGameObjectWithTag("CommonPowerUp").transform;
        //enemyPowerUp = GameObject.FindGameObjectWithTag("EnemyPowerUp").transform;
        nav = GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start()
    {
        randomIndex = Random.Range(0, escapePoints.Length);
        currentState = EnemyState.SEARCHING;

        InvokeRepeating("CheckForNullPlayers", 0.5f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (canLookForPlayers)
        {
            StartCoroutine("LookForPlayers");
        }

        //else if (canLookForPowerUps)
        //{
        //    StartCoroutine("LookForPowerUps");
        //}

        switch (currentState)
        {
            case EnemyState.SEARCHING:
                nav.speed = searchingSpeed;
                nav.SetDestination(foodStorage.position);
                break;

            case EnemyState.RUNNING:
                nav.ResetPath();
                nav.speed = runningSpeed;
                currentEscapePoint = escapePoints[randomIndex];
                nav.SetDestination(currentEscapePoint.transform.position);
                break;

            case EnemyState.FIGHTING:
                canLookForPowerUps = false;
                StopCoroutine("LookForPowerUps");
                nav.ResetPath();
                break;

            //case EnemyState.FOUNDCOMMONPOWERUP:
            //    canLookForPlayers = false;
            //    StopCoroutine("LookForPlayers");
            //    nav.ResetPath();
            //    nav.speed = runningSpeed;
            //    nav.SetDestination(commonPowerUp.position);
            //    break;

            //case EnemyState.FOUNDENEMYPOWERUP:
            //    canLookForPlayers = false;
            //    StopCoroutine("LookForPlayers");
            //    nav.ResetPath();
            //    nav.speed = runningSpeed;
            //    nav.SetDestination(enemyPowerUp.position);
            //    break;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "FightCollider")
        {
            currentState = EnemyState.FIGHTING;
            player = col.transform.root;
            players.Add(player);
        }

        //else if (col.gameObject.tag == "CommonPowerUp")
        //{
        //    Debug.Log("Took common power-up");
        //    Destroy(col.gameObject);
        //    currentState = EnemyState.SEARCHING;
        //    canLookForPlayers = true;
        //    canLookForPowerUps = true;
        //}

        //else if (col.gameObject.tag == "EnemyPowerUp")
        //{
        //    Debug.Log("Took enemy power-up");
        //    Destroy(col.gameObject);
        //    currentState = EnemyState.SEARCHING;
        //    canLookForPlayers = true;
        //    canLookForPowerUps = true;
        //}
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "FightCollider")
        {
            player = col.transform.root;
            players.Remove(player);
        }
    }

    IEnumerator LookForPlayers()
    {
        canLookForPlayers = false;

        //Only performs the distance and direction checks if there are player transforms around
        if (!player)
        {
            //Debug.Log("No player transforms found.");
            currentState = EnemyState.SEARCHING;
            canLookForPowerUps = true;
        }

        else
        {
            distanceToPlayer = (player.position - transform.position).magnitude;

            //Sets state to SEARCHING only if there are no players in the players list
            if (distanceToPlayer > runningDistance && players.Count <= 0)
            {
                currentState = EnemyState.SEARCHING;
                canLookForPowerUps = true;
            }

            //Sets state to RUNNING only if there are no players in the players list
            else if (distanceToPlayer > fightDistance && distanceToPlayer < runningDistance && players.Count <= 0)
            {
                currentState = EnemyState.RUNNING;
            }

            else if (distanceToPlayer < fightDistance)
            {
                currentState = EnemyState.FIGHTING;
            }
        }

        yield return new WaitForSeconds(1.0f);
        //Debug.Log("Looked for players");

        canLookForPlayers = true;
    }

    IEnumerator LookForPowerUps()
    {
        canLookForPowerUps = false;

        if (commonPowerUp)
        {
            distanceToCommonPowerUp = (commonPowerUp.position - transform.position).magnitude;

            if (distanceToCommonPowerUp < powerUpDistance)
            {
                Debug.Log("Found common power-up");
                currentState = EnemyState.FOUNDCOMMONPOWERUP;
            }
        }

        else if (enemyPowerUp)
        {
            distanceToEnemyPowerUp = (enemyPowerUp.position - transform.position).magnitude;

            if (distanceToEnemyPowerUp < powerUpDistance)
            {
                Debug.Log("Found enemy power-up");
                currentState = EnemyState.FOUNDENEMYPOWERUP;
            }
        }

        yield return new WaitForSeconds(1.0f);
        Debug.Log("Looked for power-ups");
        canLookForPowerUps = true;
    }

    //Checks for players that didn't leave by activating OnTriggerExit.
    //Instances include Player death and exiting by network failure etc.
    void CheckForNullPlayers()
    {
        int originalCount = players.Count;

        //Finds the first null, removes -> runs while it finds nulls
        while (players.Remove(null)) ;

        if (players.Count != originalCount)
        {
            Debug.Log("Removed " + (originalCount - players.Count) + " null players.");
        }

        //Debug.Log("Players found: " + players.Count);
    }
}
