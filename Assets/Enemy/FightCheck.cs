﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightCheck : MonoBehaviour
{
	StateController controller;

	private void Start()
	{
		controller = GetComponentInParent<StateController>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			if (!controller.playersList.Contains(other.transform.root))
			{
				controller.playersList.Add(other.transform.root);
			}

			if (!controller.playerTarget)
			{
				controller.playerTarget = other.transform.root;
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			if (controller.playersList.Contains(other.transform.root))
			{
				controller.playersList.Remove(other.transform.root);
				Debug.Log("Target left through trigger exit, removed from list");
				Debug.Log("Number of players in list: " + controller.playersList.Count);
			}
		}
	}
}
