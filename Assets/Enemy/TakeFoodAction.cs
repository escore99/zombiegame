﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "Enemy/Actions/TakeFood")]
public class TakeFoodAction : Action {

    public override void Act(StateController controller)
    {
        TakeFood(controller);
    }

    //Chooses an escape point and runs to it
    private void TakeFood (StateController controller)
    {
        if (!controller.escapePointChosen)
        {
            
            int randomIndex = 0;
            randomIndex = Random.Range(0, controller.escapePoints.Length);
            controller.currentEscapePoint = controller.escapePoints[randomIndex];
            //Debug.Log("Escape point: " + controller.currentEscapePoint.name);
            controller.escapePointChosen = true;
            //Debug.Log("Running with food");
        }

        if(!controller.HasFood)
        {
            if (GameData.Instance.NetworkMultiplayer)
            {
                if (controller.netFoodRemaining.foodAmount > 0.0f)
                {
                    controller.netFoodRemaining.TakeFood(10);
                    controller.HasFood = true;
                }
                else
                {
                    controller.HasFood = true;
                    controller.NoFoodLeft = true;
                }
            }
            else
            {
                if (controller.foodRemaining.foodAmount > 0.0f)
                {
                    controller.foodRemaining.TakeFood(10);
                    controller.HasFood = true;
                }
                else
                {
                    controller.HasFood = true;
                    controller.NoFoodLeft = true;
                }
            }
        }

        controller.navMeshAgent.ResetPath();
        controller.navMeshAgent.speed = controller.enemyStats.runningSpeed;

        Vector3 escapepos = controller.currentEscapePoint.transform.position;
        escapepos.y = controller.Map.SampleHeight(escapepos);

        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(controller.transform.position, escapepos, NavMesh.AllAreas, path);
        controller.navMeshAgent.SetPath(path);
        //controller.navMeshAgent.SetDestination(controller.currentEscapePoint.transform.position);
    }
}
