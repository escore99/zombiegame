﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/StopRunning")]
public class StopRunningDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        bool enemyEscaped = Look(controller);
        return enemyEscaped;
    }

    //Stops running if gets close enough to the escape point
    private bool Look(StateController controller)
    {
        if ( (controller.currentEscapePoint.transform.position - controller.transform.position).magnitude < controller.enemyStats.destinationReachedDistance)
        {
            Debug.Log("Enemy escaped");

            //Remove if you want the enemy to choose an escape point at the beginning and stick to it
            //Otherwise chooses a new point every time a decision to run has been made
            controller.escapePointChosen = false;

            return true;
        }

        else
        {
            return false;
        }
    }
}
