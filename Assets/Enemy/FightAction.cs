﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Actions/Fight")]
public class FightAction : Action
{
	public override void Act(StateController controller)
	{
		Fight(controller);
	}

	private void Fight(StateController controller)
	{
		controller.navMeshAgent.ResetPath();
		controller.timer += Time.deltaTime;

		if (controller.playersList.Count > 0)
		{
			controller.currentPlayerTarget = controller.playersList[0];
		}

		if (controller.currentPlayerTarget)
		{
			Vector3 direction = controller.currentPlayerTarget.position - controller.transform.position;
			direction.Normalize();
			controller.transform.rotation = Quaternion.Slerp(controller.transform.rotation,
																Quaternion.LookRotation(direction),
																controller.enemyStats.turningSpeed * Time.deltaTime);
		}

		foreach (Transform player in controller.playersList)
		{
			if (GameData.Instance.NetworkMultiplayer)
			{
				controller.playerTarget = player;

				if (controller.timer >= controller.enemyStats.attackRate &&
					controller.GetComponent<NetEnemyHealth>().health > 0 &&
					(controller.playerTarget.transform.position - controller.transform.position).magnitude < controller.enemyStats.fightDistance)
				{
					controller.timer = 0f;
					int dmg = controller.enemyStats.attackDamage;

					if (controller.playerTarget.GetComponent<NetPlayerHealth>().health > 0)
					{
						controller.playerTarget.GetComponent<NetPlayerHealth>().TakeDamage(dmg);
						Debug.Log("Player took " + dmg + " damage");
                        if (controller.Ani != null && controller.Ani.enabled)
                            controller.Ani.Play("AttackState", 0);
                    }
				}
			}
			else
			{
				controller.playerTarget = player;

				if (controller.timer >= controller.enemyStats.attackRate &&
					controller.GetComponent<EnemyHealth>().health > 0 &&
					(controller.playerTarget.position - controller.transform.position).magnitude < controller.enemyStats.fightDistance)
				{
					controller.timer = 0f;
					int dmg = controller.enemyStats.attackDamage;

					if (controller.playerTarget.GetComponent<PlayerHealth>().health > 0)
					{
						controller.playerTarget.GetComponent<PlayerHealth>().TakeDamage(dmg);
						Debug.Log("Player took " + dmg + " damage");
                        if(controller.Ani != null && controller.Ani.enabled)
                            controller.Ani.Play("AttackState", 0);
                    }
				}
			}
		}
	}
}

//speed = other.GetComponent<StateController>().enemyStats.searchingSpeed;
//other.GetComponent<StateController>().StartCoroutine(UsePowerUp(speedMultiplier, stunDuration, powerUpLength));
