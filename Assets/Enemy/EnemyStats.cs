﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Enemy/EnemyStats")]
public class EnemyStats : ScriptableObject {

    public float searchingSpeed = 2f;
    public float runningSpeed = 8f;

    public float runningDistance = 60f;
    public float fightDistance = 5f;
    public float powerUpDistance = 20f;
    public float destinationReachedDistance = 10f;
    public float turningSpeed = 5f;

    public int attackDamage = 10;
    public float attackRate = 3f;
    public float attackRange = 1f;

    public float lookRange = 15f;
    public float lookSphereCastRadius = 15f;

    //For CapsuleCast testing - didn't work
    //public float lookCapsuleCastRadius = 1f;
    //public Vector3 capsuleCastPoint1 = new Vector3(0, 1, 1);
    //public Vector3 capsuleCastPoint2 = new Vector3(0, -1, 1);

}
