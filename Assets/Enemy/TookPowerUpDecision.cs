﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/TookPowerUp")]
public class TookPowerUpDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool powerUpTaken = TookPowerUp(controller);
        return powerUpTaken;
    }

    private bool TookPowerUp(StateController controller)
    {
        PowerUp powerUp = controller.powerUpTarget.GetComponent<PowerUp>();

		if (powerUp)
		{
			if (powerUp.enemyTookPowerUp)
			{
				Debug.Log("Took PowerUp");
				return true;
			}
		}

		return false;
    }
}
