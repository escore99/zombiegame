﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/PlayerLook")]
public class PlayerLookDecision : Decision
{
	public override bool Decide(StateController controller)
	{
		bool playerVisible = Look(controller);
		return playerVisible;
	}

	private bool Look(StateController controller)
	{
		RaycastHit hit;

		Debug.DrawRay(controller.eyes.position, controller.eyes.forward.normalized * controller.enemyStats.lookRange, Color.green);

		if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit,
			controller.enemyStats.lookRange)
			&& hit.collider.CompareTag("Player"))
		{
			if (!hit.transform.root.gameObject.activeSelf)
			{
				return false;
			}
			else
			{
				Debug.Log("Saw player, running");

				if (!controller.playerTarget)
				{
					controller.playerTarget = hit.transform;
				}
		
				//If the enemy sees a player, that player is added to the list (if the player wasn't in the list before)
				//This list is checked in FightDecision and StopFightingDecision
				if (!controller.playersList.Contains(hit.transform))
				{
					controller.playersList.Add(hit.transform);
				}

				Debug.Log("Number of players in list: " + controller.playersList.Count);
				return true;
			}
		}
		else
		{
			return false;
		}
	}
}
