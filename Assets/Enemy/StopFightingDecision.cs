﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/StopFighting")]
public class StopFightingDecision : Decision
{
	public override bool Decide(StateController controller)
	{
		bool shouldContinueFighting = Look(controller);
		return shouldContinueFighting;
	}

	//Checks if any of the seen players are in the list OR if a current target is still at fighting distance
	private bool Look(StateController controller)
	{
		if (controller.playersList.Count <= 0)
		{
			return true;
		}
		else
		{
			for (int i = 0; i < controller.playersList.Count; ++i)
			{
				if ((controller.playersList[i].transform.position - controller.transform.position).magnitude > controller.enemyStats.fightDistance
					|| !controller.playersList[i].gameObject.activeSelf)
				{
					if (controller.playersList.Contains(controller.playersList[i]))
					{
						controller.playersList.Remove(controller.playersList[i]);
						Debug.Log("Target not at fighting distance, removed from list");
						Debug.Log("Number of players in list: " + controller.playersList.Count);
					}

					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}
}
