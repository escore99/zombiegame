﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "Enemy/Actions/Run")]
public class RunAction : Action
{
    public override void Act(StateController controller)
    {
        Run(controller);
    }

    //First chooses an escape point and then runs to it
    private void Run(StateController controller)
    {
        if (!controller.escapePointChosen)
        {
            int randomIndex = 0;
            randomIndex = Random.Range(0, controller.escapePoints.Length);
            controller.currentEscapePoint = controller.escapePoints[randomIndex];
            Debug.Log("Escape point: " + controller.currentEscapePoint.name);
            controller.escapePointChosen = true;
        }

        if (controller.Ani != null && controller.Ani.enabled && !controller.Ani.GetCurrentAnimatorStateInfo(0).IsName("RunState"))
        {
            controller.Ani.Play("RunState", 0);
            //controller.Ani.Play()
        }

        controller.navMeshAgent.ResetPath();
        controller.navMeshAgent.speed = controller.enemyStats.runningSpeed;
        Vector3 escapepos = controller.currentEscapePoint.transform.position;
        escapepos.y = controller.Map.SampleHeight(escapepos);
        
        //Debug.Log("Calculating path");
        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(controller.transform.position, escapepos, NavMesh.AllAreas, path);
        controller.navMeshAgent.SetPath(path);
        //Debug.Log("Path set: " + p + " - " + path.status + " - " + path.corners.Length);
        //controller.navMeshAgent.SetDestination(controller.currentEscapePoint.transform.position);
    }
}
