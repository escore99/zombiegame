﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "Enemy/Actions/Search")]
public class SearchAction : Action
{
    public override void Act(StateController controller)
    {
        Search(controller);
    }

    //Default action in the beginning
    private void Search(StateController controller)
    {
        controller.navMeshAgent.ResetPath();
        controller.navMeshAgent.speed = controller.enemyStats.searchingSpeed;
        //controller.navMeshAgent.SetDestination(controller.foodStorage.position);

        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(controller.transform.position, controller.foodStorage.position, NavMesh.AllAreas, path);
        controller.navMeshAgent.SetPath(path);


        if (controller.Ani != null && controller.Ani.enabled && !controller.Ani.GetCurrentAnimatorStateInfo(0).IsName("RunState"))
        {
            controller.Ani.Play("RunState", 0);
            //controller.Ani.Play()
        }
        //Debug.Log(path.status);
    }
}
