﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : MonoBehaviour
{
    public State currentState;
    public State remainState;
    public State runningState;
    public EnemyStats enemyStats;
	public Transform eyes;
    public bool escapePointChosen = false;
    public SoundManager sm;
    public AudioSource afraidSource;
    public AudioSource breathingSource;
    public FoodRemaining foodRemaining;
    public NetFoodRemaining netFoodRemaining;
    public Terrain Map;

    private AudioClip afraidSound;
    private AudioClip breath;
    private bool breathingDisabled = true;
    public bool HasFood = false;
    public bool NoFoodLeft = false;
    public Animator Ani;

    [HideInInspector]    public GameObject[] escapePoints;
    [HideInInspector]    public GameObject currentEscapePoint;
    [HideInInspector]    public Transform foodStorage;
    [HideInInspector]    public NavMeshAgent navMeshAgent;
    [HideInInspector]    public Transform playerTarget;
	[HideInInspector]	 public Transform currentPlayerTarget;
	[HideInInspector]    public Transform powerUpTarget;
    [HideInInspector]    public List<Transform> playersList = new List<Transform>();
    [HideInInspector]    public float stateTimeElapsed;
    [HideInInspector]    public float timer;

    void Awake ()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        foodStorage = GameObject.FindGameObjectWithTag("FoodStorage").transform;
        escapePoints = GameObject.FindGameObjectsWithTag("EscapePoint");
        afraidSource = GetComponent<AudioSource>();
        foodRemaining = FindObjectOfType<FoodRemaining>();
        netFoodRemaining = FindObjectOfType<NetFoodRemaining>();
        Map = FindObjectOfType<Terrain>();
        Ani = GetComponentInChildren<Animator>();
    }

    void Start()
    {
        //InvokeRepeating("CheckForNullPlayers", 0.5f, 0.5f);

        if (!navMeshAgent.isOnNavMesh)
        {
            navMeshAgent.enabled = false;
            navMeshAgent.enabled = true;
        }
    }

    void Update ()
    {
		CheckForNullPlayers();

        if (breathingDisabled)
        {
            //breath = sm.GetSound("enemyBreathing");
            //breathingSource.PlayOneShot(breath, 1f);
            //StartCoroutine(WaitBreathing(2));
        }

        if(HasFood)
        {
            if((currentEscapePoint.transform.position-transform.position).magnitude < 5)
            {
                // Escaped
                if(GameData.Instance.NetworkMultiplayer)
                {
                    FindObjectOfType<NetGameController>().EnemyEscaped();
                    GetComponent<NetEnemyHealth>().DestroySelf();
                }
                else
                {
                    // Check if game over somewhere!
                    Destroy(gameObject);
                }
            }
        }

        currentState.UpdateState(this);
    }

    void OnDrawGizmos()
    {
        if (currentState != null && eyes != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawWireSphere(eyes.position, enemyStats.lookSphereCastRadius);
        }

        if (currentState != null && eyes != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawWireSphere(eyes.position, enemyStats.lookSphereCastRadius);
        }
    }

    public void TransitionToState (State nextState)
    {
        if (nextState != remainState)
        {
            currentState = nextState;
        }
        if (currentState == runningState)
        {
            //afraidSound = sm.GetSound("enemyAfraid");
            //afraidSource.PlayOneShot(afraidSound, 1f);
        }
    }

    public bool CheckIfCountDownElapsed (float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
    }
    
    private void OnExitState()
    {
        stateTimeElapsed = 0;
    }

    private void CheckForNullPlayers()
    {
        int originalCount = playersList.Count;

        //Finds the first null, removes -> runs while it finds nulls
        while (playersList.Remove(null)) ;

        if (playersList.Count != originalCount)
        {
            Debug.Log("Removed " + (originalCount - playersList.Count) + " null players.");
        }

        //Debug.Log("Players found: " + players.Count);
    }

    public IEnumerator UsePowerUp(float multiplier, float stunDuration, float length)
    {
        float runningSpeed = enemyStats.runningSpeed;
        float searchingSpeed = enemyStats.searchingSpeed;
        float turningSpeed = enemyStats.turningSpeed;

        if (stunDuration > 0)
        {
            // Setting movement to 0 for the stun duration
            runningSpeed = 0;
            searchingSpeed = 0;
            turningSpeed = 0;
            yield return new WaitForSeconds(stunDuration);
            
        }
        // Setting the speed to the multiplied amount
        enemyStats.runningSpeed = runningSpeed;// * multiplier;
        enemyStats.searchingSpeed = searchingSpeed;// * multiplier;
        enemyStats.turningSpeed = turningSpeed;// * multiplier;
        // Waiting for the powerup to end
        yield return new WaitForSeconds(length - stunDuration);
        // Setting the speed back to normal value
        //enemyStats.runningSpeed /= multiplier;
        //enemyStats.searchingSpeed /= multiplier;
        //enemyStats.turningSpeed /= multiplier;
    }

    IEnumerator WaitBreathing(float time)
    {
        breathingDisabled = false;
        yield return new WaitForSeconds(time);
        breathingDisabled = true;
    }
}
