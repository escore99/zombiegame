﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/Fight")]
public class FightDecision : Decision
{
	public override bool Decide(StateController controller)
	{
		bool playerAtFightingDistance = Look(controller);
		return playerAtFightingDistance;
	}

	//Checks the distance to all players in the list of seen players and starts fighting if they're close enough
	//Still have to come up with a logic for players that attack the enemy from behind, because in those
	//cases the enemy doesn't see them, but should still realize that it's fighting
	private bool Look(StateController controller)
	{
		if (controller.playersList.Count > 0)
		{
			for (int i = 0; i < controller.playersList.Count; ++i)
			{
				if (!controller.playersList[i].gameObject.activeSelf)
				{
					return false;
				}
				else if ((controller.playersList[i].transform.position - controller.transform.position).magnitude <= controller.enemyStats.fightDistance)
				{
					Debug.Log("Player caught me, fighting");
                    if (controller.Ani != null && controller.Ani.enabled && 
                        !controller.Ani.GetCurrentAnimatorStateInfo(0).IsName("IdleState") && !controller.Ani.GetCurrentAnimatorStateInfo(0).IsName("AttackState"))
                    {
                        controller.Ani.Play("IdleState", 0);
                        //controller.Ani.Play()
                    }
                    //else
                    //{
                    //    Ani.Play(animation, 0);
                    //}
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}
}
