﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/PowerUpLook")]
public class PowerUpLookDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool powerUpVisible = Look(controller);
        return powerUpVisible;
    }
	
    private bool Look(StateController controller)
    {
        RaycastHit hit;

        Debug.DrawRay(controller.eyes.position, controller.eyes.forward.normalized * controller.enemyStats.lookRange, Color.magenta);

        if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit,
            controller.enemyStats.lookRange)
            && hit.collider.CompareTag("CommonPowerUp"))
        {
			if (!hit.transform.root.gameObject.activeSelf)
			{
				return false;
			}
			else
			{
				Debug.Log("Saw Common PowerUp");
				controller.powerUpTarget = hit.transform; //passed to GoToPowerUpAction, PowerUpRemovedDecision and TookPowerUpDecision
				return true;
			}

        }

        else if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit,
                    controller.enemyStats.lookRange)
                    && hit.collider.CompareTag("EnemyPowerUp"))
        {
			if (!hit.transform.root.gameObject.activeSelf)
			{
				return false;
			}
			else
			{
				Debug.Log("Saw Enemy PowerUp");
				controller.powerUpTarget = hit.transform; //passed to GoToPowerUpAction, PowerUpRemovedDecision and TookPowerUpDecision
				return true;
			}
        }
        else
        {
            return false;
        }
    }
}
