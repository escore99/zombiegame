﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/AtFoodStorage")]
public class AtFoodStorageDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool gotToFoodStorage = Look(controller);
        return gotToFoodStorage;
    }

    private bool Look (StateController controller)
    {
        if ((controller.foodStorage.transform.position - controller.transform.position).magnitude < controller.enemyStats.destinationReachedDistance)
        {
            Debug.Log("Reached food storage");
            return true;
        }

        else
        {
            return false;
        }
    }
}
