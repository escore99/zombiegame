﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/Decisions/PowerUpRemoved")]
public class PowerUpRemovedDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool powerUpRemoved = CheckForRemovedPowerUps(controller);
        return powerUpRemoved;
    }

    private bool CheckForRemovedPowerUps(StateController controller)
    {
        if (!controller.powerUpTarget || !controller.powerUpTarget.gameObject.activeSelf)
        {
            return true;
        }

        else
        {
            return false;
        }
    }
}
