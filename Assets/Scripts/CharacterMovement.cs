﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {

    public int stun = 1;
    public bool playerOne;
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public float rotationSpeed = 90.0F;
    public float mouseRotationSpeed = 150.0F;
    public CharacterController controller;
    public ParticleSystem dust;
    public SoundManager sm;
    public AudioSource breathingSource;
    public AudioSource footstepSource;
    public AudioSource jumpSource;
    public AudioSource fallingSource;

    public Animator Ani;
    public NetAnimator NetAni;

    private Vector3 moveDirection = Vector3.zero;
    private string mouseX, horizontal, horizontalMouselook, vertical, rotate;
    private Vector3 lastPos;
    private bool breathingDisabled = true;
    private bool footstepsDisabled = true;
    private AudioClip breath;
    private AudioClip footstep;
    private AudioClip jump;
    private AudioClip falling;


    void Awake()
    {
        breathingSource = GetComponent<AudioSource>();
        footstepSource = GetComponent<AudioSource>();
        jumpSource = GetComponent<AudioSource>();
        fallingSource = GetComponent<AudioSource>();
    }
    void Start()
    {
        if (playerOne)
        {
            mouseX = "Mouse X";
            horizontal = "Horizontal";
            horizontalMouselook = "HorizontalMouselook";
            vertical = "Vertical";
            rotate = "Rotate";
            lastPos = gameObject.transform.position;
        }
        else
        {
            mouseX = "Mouse X";
            horizontal = "P2Horizontal";
            horizontalMouselook = "HorizontalMouselook";
            vertical = "P2Vertical";
            rotate = "P2Rotate";
        }
    }
    void Update()
    {
        
        //CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            if (breathingDisabled)
            {
               /* breath = sm.GetSound("playerBreathing");
                breathingSource.PlayOneShot(breath, 1f);
                StartCoroutine(WaitBreathing(2));*/
            }
            if (Input.GetMouseButton(1))
            {
                transform.Rotate(0, Input.GetAxis(mouseX) * mouseRotationSpeed * Time.deltaTime * stun, 0);
                moveDirection = new Vector3(Input.GetAxis(horizontalMouselook), 0, Input.GetAxis(vertical));
            }
            else
            {
                transform.Rotate(0, Input.GetAxis(rotate) * rotationSpeed * Time.deltaTime * stun, 0);
                moveDirection = new Vector3(Input.GetAxis(horizontal), 0, Input.GetAxis(vertical));
            }
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed * stun;
            if (lastPos != gameObject.transform.position)
            {
                if(GameData.Instance.NetworkMultiplayer)
                {
                    NetAni.PlayAnimation("RunState");
                }
                else
                {
                    if(Ani != null && !Ani.GetCurrentAnimatorStateInfo(0).IsName("RunState") && !Ani.GetCurrentAnimatorStateInfo(0).IsName("AttackState"))
                    {
                        Ani.Play("RunState", 0);
                    }
                }
                dust.Play();
                lastPos = gameObject.transform.position;
                if (footstepsDisabled)
                {
                    footstep = sm.GetSound("footSteps");
                    footstepSource.PlayOneShot(footstep,0.1f);
                    StartCoroutine(WaitFootsetps(0.3f));
                }
                
            }
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
                jump = sm.GetSound("jump");
                jumpSource.PlayOneShot(jump, 1f);
            }
        }
        else
        {
            if (Input.GetMouseButton(1))
            {
                transform.Rotate(0, Input.GetAxis(mouseX) * mouseRotationSpeed * Time.deltaTime * stun, 0);
            }
            else
            {
                transform.Rotate(0, Input.GetAxis(rotate) * rotationSpeed * Time.deltaTime * stun, 0);
            }
            // Ei kai näitä joka framella haluta aloittaa?
            //StartCoroutine(WaitFalling());
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
        
        //transform.Translate(moveDirection * Time.deltaTime*5);
    }

    public IEnumerator UsePowerUp(float multiplier, float stunDuration, float length)
    {
        speed *= multiplier;
        if (stunDuration > 0)
        {
            // Stun multiplies the players movement. 
            stun = 0;
            yield return new WaitForSeconds(stunDuration);
            stun = 1;
        }
        yield return new WaitForSeconds(length - stunDuration);
        speed /= multiplier;
    }

    IEnumerator WaitBreathing(float time)
    {
        breathingDisabled = false;
        yield return new WaitForSeconds(time);
        breathingDisabled = true;
    }

    IEnumerator WaitFootsetps(float time)
    {
        footstepsDisabled = false;
        yield return new WaitForSeconds(time);//WaitWhile (()=> source.isPlaying
        footstepsDisabled = true;
    }

    IEnumerator WaitFalling()
    {
        while (!controller.isGrounded)
            yield return null;
        falling = sm.GetSound("fallToGround");
        fallingSource.PlayOneShot(falling, 0.07f);
        //yield return new WaitForSeconds(time);//WaitWhile (()=> source.isPlaying
    }



}
