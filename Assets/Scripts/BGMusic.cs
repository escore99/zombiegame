﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMusic : MonoBehaviour {

    public AudioSource BGMusicSource;
    public SoundManager sm;

    void Awake()
    {
        BGMusicSource = GetComponent<AudioSource>();
        BGMusicSource.clip = sm.GetSound("BGMusic");
        BGMusicSource.Play();
    }
}
