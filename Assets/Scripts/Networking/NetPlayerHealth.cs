﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetPlayerHealth : Photon.PunBehaviour {

    public int health;
    public Image healthBar;
    public float healthLossCooldown;
    public ParticleSystem blood;

    public float Fill;

    private int maxHealth;
    private bool healthLossDisabled;

    // Use this for initialization
    void Start()
    {
        Fill = 1.0f;
        health = 100;
        maxHealth = health;
        if (healthBar == null)
        {
            healthBar = GameObject.Find("PlayerHealth").GetComponent<Image>();
        }
    }
    
    public override void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        //Assign this gameObject to player called instantiate the prefab
        info.sender.TagObject = this;
    }

    public void TakeDamage(int amount)
    {
        if(PhotonNetwork.isMasterClient)
        {
            photonView.RPC("NetTakeDamage", photonView.owner, amount);
            //photonView.RPC("NetSetFill", PhotonTargets.All, (float)health / (float)maxHealth);
            //photonView.RPC("NetTakeDamage", PhotonTargets.All, amount);
        }
    }

    public void Heal(int amount)
    {
        health += amount;
        if (health > maxHealth)
            health = maxHealth;
        healthBar.fillAmount = (float)health / (float)maxHealth;
        Fill = healthBar.fillAmount;
    }

    [PunRPC]
    public void NetTakeDamage(int amount)
    {
        if (!healthLossDisabled)
        {
            health -= amount;
            Fill = (float)health / (float)maxHealth;
            blood.Play();
            if (healthBar != null)
            {
                healthBar.fillAmount = (float)health / (float)maxHealth;
            }

            if (health <= 0)
            {
                Fill = 0.0f;
                //gameObject.SetActive(false); // PhotonView disablointi huono homma
                //PhotonNetwork.Destroy(gameObject);
                //GameObject.Find("SpecCamera").GetComponent<Camera>().enabled = true;
                NetworkManager net = FindObjectOfType<NetworkManager>();
                if (net.PlayerLives > 0)
                {
                    Transform newPos = net.PlayerSpawnPoints[Random.Range(0, net.PlayerSpawnPoints.Length)];
                    transform.position = newPos.position;
                    transform.rotation = newPos.rotation;
                    photonView.RPC("PlayerDeath", PhotonTargets.All);
                    Fill = 1.0f;
                    health = maxHealth;
                    healthBar.fillAmount = (float)health / (float)maxHealth;
                }
                else
                {
                    // Cant respawn, turn on spec camera
                    GetComponent<CharacterController>().enabled = false;
                    GetComponent<CharacterMovement>().enabled = false;
                    transform.position = new Vector3(-1000f, -1000f, -1000f);
                    GetComponentInChildren<Camera>().enabled = false;
                    GameObject.Find("SpecCamera").GetComponent<Camera>().enabled = true;
                    photonView.RPC("PlayerDeath", PhotonTargets.All);
                }
            }
        }
        StartCoroutine(DisableHealthLoss(healthLossCooldown));
    }

    [PunRPC]
    public void PlayerDeath()
    {
        FindObjectOfType<NetworkManager>().PlayerDeath();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // Our player. Send position to network
            stream.SendNext(Fill);
        }
        else
        {
            // Someone else's player. We receive this position info
            Fill = (float)stream.ReceiveNext();
        }
    }

    IEnumerator DisableHealthLoss(float time)
    {
        yield return new WaitForSeconds(0.01f);
        healthLossDisabled = true;
        yield return new WaitForSeconds(time);
        healthLossDisabled = false;
    }
}
