﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetGameController : Photon.PunBehaviour
{
    public string enemyResourceName;
    public Transform[] enemySpawnPoints1;
    public Transform[] enemySpawnPoints2;
    public Transform[] enemySpawnPoints3;
    private Transform[] currentSpawnPoints;
    public bool spawnEnemy;
    public float enemyCooldown = 0;
    public Text nextWave;
    public Text status;
    public int enemiesRemaining = 0;
    public SoundManager sm;
    public Terrain Map;

    private bool firstRound = true;
    private int level = 1;
    private int enemyCounter = 0;
    private int time;

    private NetFoodRemaining foodRemaining;
    private NetworkManager netManager;
    
    public int Level
    {
        get { return level; }
    }
    public int Time
    {
        get { return time; }
    }

    // Use this for initialization
    void Start ()
    {
        foodRemaining = FindObjectOfType<NetFoodRemaining>();
        netManager = GetComponent<NetworkManager>();
        status.supportRichText = true;
        currentSpawnPoints = enemySpawnPoints1;
        //StartCoroutine(NextWave(5));
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (PhotonNetwork.isMasterClient && enemyCounter >= 0 && spawnEnemy)
        {
            StartCoroutine(DisableSpawn(enemyCooldown));
            SpawnEnemy();
        }
    }

    // RPC: StartWave, EndWave
    void SpawnEnemy()
    {
        if (PhotonNetwork.isMasterClient)
        {
            int spawnpoint = Random.Range(0, currentSpawnPoints.Length);
            Vector3 spawnpos = currentSpawnPoints[spawnpoint].position;
            //spawnpos.y = Map.SampleHeight(spawnpos);
            
            PhotonNetwork.InstantiateSceneObject("NetEnemy", spawnpos, currentSpawnPoints[spawnpoint].rotation, 0, null);
            enemyCounter--;
            enemiesRemaining++;
            UpdateStatus();
        }
    }

    IEnumerator DisableSpawn(float time)
    {
        spawnEnemy = false;
        yield return new WaitForSeconds(time);
        spawnEnemy = true;
    }

    IEnumerator NextWave(int time)
    {
        int min, sec;
        this.time = time;

        while (time >= 0)
        {
            sec = time % 60;
            min = (time - sec) / 60;
            // Debug.Log(min + ":" + sec);
            if (firstRound)
            {
                if (sec < 10)
                {
                    nextWave.text = "First round in: \n" + min + ":0" + sec;
                }
                else
                {
                    nextWave.text = "First round in: \n" + min + ":" + sec;
                }
            }
            else
            {
                if (sec < 10)
                {
                    nextWave.text = "Next wave in: \n" + min + ":0" + sec;
                }
                else
                {
                    nextWave.text = "Next wave in: \n" + min + ":" + sec;
                }
            }
            yield return new WaitForSeconds(1);
            time -= 1;
            this.time = time;

        }

        if (firstRound)
        {
            firstRound = false;
        }
        else
        {
            level++;
        }

        if (level < 3)
        {
            currentSpawnPoints = enemySpawnPoints1;
        }
        else if (level < 5)
        {
            currentSpawnPoints = enemySpawnPoints2;
        }
        else if (level >= 5)
        {
            currentSpawnPoints = enemySpawnPoints3;
        }
        spawnEnemy = true;
        enemyCounter = 10 + level * 2;
        StartCoroutine(WaveRemaining(enemyCounter * enemyCooldown));
    }

    IEnumerator WaveRemaining(float time)
    {
        float min, sec;

        spawnEnemy = true;

        while (time >= 0)
        {
            sec = time % 60;
            min = (time - sec) / 60;
            if (sec < 10)
            {
                nextWave.text = "Wave remaining: \n" + min + ":0" + sec;
            }
            else
            {
                nextWave.text = "Wave remaining: \n" + min + ":" + sec;
            }
            yield return new WaitForSeconds(1);
            time -= 1;
        }

        StartCoroutine(NextWave(35));
    }

    public void UpdateStatus()
    {
        if (PhotonNetwork.isMasterClient)
        {
            string s = "<B>Level: " + level + "</B><size= 24>\nHumans: " + enemiesRemaining + "\n" + "Zombies left: " + netManager.PlayerLives.ToString() + "</size>";
            photonView.RPC("NetUpdateStatus", PhotonTargets.All, s);
        }
    }

    // Called when new player joins to sync game clock
    [PunRPC]
    public void NetUpdateTime(int time, int currentlevel)
    {
        Debug.Log("Time: " + time.ToString() + ". Level: " + currentlevel.ToString());
        if (!PhotonNetwork.isMasterClient)
        {
            StopAllCoroutines();
            level = currentlevel;
            StartCoroutine(NextWave(time));
        }
    }

    [PunRPC]
    public void NetUpdateStatus(string newstatus)
    {    
        status.text = newstatus;
    }

    public override void OnJoinedRoom()
    {
        status.supportRichText = true;
        currentSpawnPoints = enemySpawnPoints1;
        //spawnEnemy = true;
        //enemyCounter = 10;
        StartCoroutine(NextWave(5));
    }

    public void EnemyEscaped()
    {
        Debug.Log("Enemy escaped");
        if (PhotonNetwork.isMasterClient)
        {
            enemiesRemaining--;
            UpdateStatus();
        }
        StopCoroutine("FoodGameOverCheck");
        StartCoroutine(FoodGameOverCheck());
    }

    public IEnumerator FoodGameOverCheck()
    {
        
        while (true)
        {
            if(foodRemaining.foodAmount > 0.0f)
            {
                break;
            }
            bool enemyWithFood = false;
            foreach (var enemy in FindObjectsOfType<StateController>())
            {
                if (enemy.HasFood && !enemy.NoFoodLeft)
                    enemyWithFood = true;
            }
            if(!enemyWithFood)
            {
                //Game over
                Debug.Log("Game over, no more food left");
                FindObjectOfType<NetworkManager>().GameOver();
                break;
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

}
