﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetFoodRemaining : Photon.MonoBehaviour
{
    
    public Text food;
    public float foodAmount;
    public Transform FoodCart;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void TakeFood(float amount)
    {
        //foodAmount -= amount;
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("NetTakeFood", PhotonTargets.All, amount);
        }
    }

    [PunRPC]
    public void NetTakeFood(float amount)
    {
        foodAmount -= amount;
        ShowFoodAmount();
        Debug.Log("Taken: " + amount.ToString() + ", remaining: " + foodAmount.ToString());
    }

    public void RestoreFood(float amount, Vector3 lastPos)
    {
        if (PhotonNetwork.isMasterClient)
        {
            float dist = Vector3.Distance(FoodCart.transform.position, lastPos);
            float addAmount = Mathf.Clamp((16f / dist) * amount, 1f, amount );
            foodAmount += addAmount;
            photonView.RPC("NetRestoreFood", PhotonTargets.Others, foodAmount);
            ShowFoodAmount();
        }
    }

    [PunRPC]
    public void NetRestoreFood(float amount)
    {
        foodAmount = amount;
        ShowFoodAmount();
    }

    public void ShowFoodAmount()
    {
        if(foodAmount > 75f)
        {
            food.text = "Food remains: 100%";
        }
        else if (foodAmount > 50f)
        {
            food.text = "Food remains: 75%";
        }
        else if (foodAmount > 25f)
        {
            food.text = "Food remains: 50%";
        }
        else if (foodAmount > 0f)
        {
            food.text = "Food remains: 25% Hurry up!";
        }
        else if (foodAmount <= 0f)
        {
            food.text = "Food remains: 0%";
        }
    }
}
