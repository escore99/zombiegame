﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetAnimator : Photon.MonoBehaviour
{
    public Animator Ani;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void PlayAnimation(string animation)
    {
        photonView.RPC("NetPlayAnimation", PhotonTargets.All, animation);
    }

    [PunRPC]
    public void NetPlayAnimation(string animation)
    {
        
        if (animation.Equals("RunState") && !Ani.GetCurrentAnimatorStateInfo(0).IsName("RunState") && !Ani.GetCurrentAnimatorStateInfo(0).IsName("AttackState"))
        {
            //Debug.Log("RunState");
            Ani.Play("RunState", 0);
        }
        else
        {
            if (!Ani.GetCurrentAnimatorStateInfo(0).IsName(animation))
                Ani.Play(animation, 0);
        }
    }
}
