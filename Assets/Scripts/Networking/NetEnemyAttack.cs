﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetEnemyAttack : Photon.MonoBehaviour {

    public float timeBetweenAttacks = 1.0f;
    public int attackDamage = 10;
    public Transform target;

    private float timer;
    private NetEnemyHealth enemyHealth;
    private EnemyMovement enemyMovement;

    void Awake()
    {
        enemyHealth = GetComponent<NetEnemyHealth>();
        enemyMovement = GetComponent<EnemyMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        foreach (Transform player in enemyMovement.players)
        {
            if (timer >= timeBetweenAttacks &&
                enemyMovement.currentState == EnemyMovement.EnemyState.FIGHTING &&
                enemyHealth.health > 0)
            {
                target = player;
                Attack(target);
            }
        }
    }

    void Attack(Transform t)
    {
        timer = 0f;

        if (t != null && t.GetComponent<NetPlayerHealth>().health > 0)
        {
            t.GetComponent<NetPlayerHealth>().TakeDamage(attackDamage);
            Debug.Log("Player took " + attackDamage + " damage.");
        }
    }
}
