﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Copypastaa vanhasta projektista
public class NetworkManager : Photon.PunBehaviour
{
    public Transform[] PlayerSpawnPoints;
    public Camera SpecCamera;
    public NetGameController NetGC;
    private const string RoomName = "ZombieGame";
    private RoomInfo[] roomList;
    public List<PhotonPlayer> CurrentPlayersInRoom = new List<PhotonPlayer>();
    private RoomOptions roomOptions;

    public GameObject RoomBrowser;
    public GameObject RoomButtonPrefab;
    public Transform RoomButtonsLayout;

    public GameObject CreateRoomPopup;
    public InputField NewRoomNameInput;

    public InputField PlayerNameInput;

    public GameObject SelectZombiePopup;
    public GameObject GameOverPopup;
    public Text GameOverText;

    private GameObject[] buttonObjects;
    private int playerLives;
    //private List<GameObject> 

    public int PlayerLives
    {
        get { return playerLives; }
    }


    // Use this for initialization
    void Start ()
    {
        GameData.Instance.SetGameMode(GameMode.NetworkMultiplayer);
        PhotonNetwork.logLevel = PhotonLogLevel.ErrorsOnly;
        PhotonNetwork.ConnectUsingSettings("0.1");
        roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;
        RoomBrowser.SetActive(true);
        PlayerNameInput.onEndEdit.AddListener(delegate { NameInput(PlayerNameInput); });
        playerLives = 8;
        PhotonNetwork.playerName = "Zombie";
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    /*
    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
        
        if (PhotonNetwork.room == null)
        {
            // Create room
            if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
            {
                //PhotonNetwork.CreateRoom(RoomName + System.Guid.NewGuid().ToString("N"));
                PhotonNetwork.CreateRoom(RoomName + System.Guid.NewGuid().ToString("N"), roomOptions, null);
            }

            // Join room
            if (roomList != null)
            {
                for (int i = 0; i < roomList.Length; i++)
                {
                    if (GUI.Button(new Rect(100, 250 + (110 * i), 250, 100), "Join" + roomList[i].Name + "\n" + roomList[i].PlayerCount + " / " + roomList[i].MaxPlayers))
                    {
                        PhotonNetwork.JoinRoom(roomList[i].Name);
                    }
                }
            }
        }

        foreach (RoomInfo game in PhotonNetwork.GetRoomList())
        {
            GUILayout.Label(game.Name + " " + game.PlayerCount + "/" + game.MaxPlayers);
        }

    }
    */
    void ConnectedToMaster()
    {
        //Debug.Log("MasterYhteys");
    }

    public override void OnReceivedRoomListUpdate()
    {
        //Debug.Log("Room list update");
        roomList = PhotonNetwork.GetRoomList();
        createRoomButtons();
    }

    public override void OnJoinedLobby()
    {
        //PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed()
    {
        //Debug.Log("Can't join random room!");
        //PhotonNetwork.CreateRoom(null);
    }

    public override void OnJoinedRoom()
    {
        // Täällä instansioidaan pelaaja, joka tulee huoneeseen
        RoomBrowser.SetActive(false);

        SelectZombiePopup.SetActive(true);

        //SpecCamera.enabled = false;
        //int point = Random.Range(0, PlayerSpawnPoints.Length);
        //GameObject player = PhotonNetwork.Instantiate("NetPlayer", PlayerSpawnPoints[point].position, PlayerSpawnPoints[point].rotation, 0);
        //if (PhotonNetwork.playerName.Length == 0)
        //    PhotonNetwork.playerName = "Zombie";
        //player.GetPhotonView().owner.NickName = PhotonNetwork.playerName;

        //PhotonView myView = player.GetPhotonView();
        //myView.ObservedComponents.Add(player.GetComponentInChildren<NetCharacterAttack>());
        //player.GetComponentInChildren<NetCharacterAttack>().View = myView;
        //if(PhotonNetwork.isMasterClient)
        //    PhotonNetwork.InstantiateSceneObject("NetEnemy", new Vector3(14, 4, 33 + Random.Range(-3,4)), Quaternion.identity, 0, null);
        
    }

    // Called when a remote player entered the room. This PhotonPlayer is already added to the playerlist at this time.
    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        if (PhotonNetwork.isMasterClient)
        {
            object[] prms = new object[2]
            {
                (object)NetGC.Time,
                (object)NetGC.Level,
            };
            // Clock set
            photonView.RPC("NetUpdateTime", newPlayer, prms);
        }
        // new player to player list

    }

    public override void OnCreatedRoom()
    {
        // Täällä voidaan luoda huoneeseen oleellisia juttuja
        // Setup vaihe huoneen tilanteelle.
    }

    public void CreateNewRoom()
    {
        CreateRoomPopup.SetActive(true);
    }

    public void CloseCreateNewRoomPopup()
    {
        CreateRoomPopup.SetActive(false);
    }

    public void SelectZombie(bool big)
    {
        SpecCamera.enabled = false;
        int point = Random.Range(0, PlayerSpawnPoints.Length);
        if(big)
        {
            GameObject player = PhotonNetwork.Instantiate("NetPlayerBig", PlayerSpawnPoints[point].position, PlayerSpawnPoints[point].rotation, 0);
            if (PhotonNetwork.playerName.Length == 0)
                PhotonNetwork.playerName = "Zombie";
            player.GetPhotonView().owner.NickName = PhotonNetwork.playerName;
        }
        else
        {
            GameObject player = PhotonNetwork.Instantiate("NetPlayer", PlayerSpawnPoints[point].position, PlayerSpawnPoints[point].rotation, 0);
            if (PhotonNetwork.playerName.Length == 0)
                PhotonNetwork.playerName = "Zombie";
            player.GetPhotonView().owner.NickName = PhotonNetwork.playerName;
        }

        SelectZombiePopup.SetActive(false);
    }

    public void StartNewGame()
    {
        if(NewRoomNameInput.text.Length == 0)
        {
            PhotonNetwork.CreateRoom(RoomName + System.Guid.NewGuid().ToString("N"), roomOptions, null);
        }
        else
        {
            // TODO tsekkaa onko samannimisiä huoneita olemassa
            PhotonNetwork.CreateRoom(RoomName + " - " + NewRoomNameInput.text, roomOptions, null);
        }
    }

    public void NameInput(InputField input)
    {
        if (input.text.Length == 0)
        {
            //photonView.owner.NickName = "Zombie";
            PhotonNetwork.playerName = "Zombie";
        }
        else
        {
            //photonView.owner.NickName = input.text;
            PhotonNetwork.playerName = input.text;
        }
    }

    public void PlayerDeath()
    {
        if (PhotonNetwork.isMasterClient)
        {
            --playerLives;
            photonView.RPC("SyncLives", PhotonTargets.All, playerLives);
            if (playerLives <= 0)
            {
                StopAllCoroutines();
                // GAME OVER?
                StartCoroutine(checkForAlivePlayers());
            }
        }
    }

    [PunRPC]
    public void SyncLives(int lives)
    {
        playerLives = lives;
        Debug.Log("Synced lives to: " + playerLives.ToString());
    }

    public void GameOver()
    {
        Debug.Log("NetworkManager Game Over");
        if(PhotonNetwork.isMasterClient)
        {
            photonView.RPC("NetGameOver", PhotonTargets.All);
        }
    }

    // Out of food game over
    [PunRPC]
    public void NetGameOver()
    {
        foreach (var cam in FindObjectsOfType<Camera>())
        {
            cam.enabled = false;
        }
        SpecCamera.enabled = true;

        GameOverPopup.SetActive(true);
        GameOverText.text = "GAME OVER\nRAN OUT OF FOOD";
        StartCoroutine(returnToMainIn(6f));
    }


    private void createRoomButtons()
    {
        if (buttonObjects != null)
        {
            foreach (var btn in buttonObjects)
            {
                Destroy(btn);
            }
        }
        buttonObjects = new GameObject[roomList.Length];
        int i = 0;
        foreach (RoomInfo room in roomList)
        {
            GameObject newbtn = Instantiate(RoomButtonPrefab, RoomButtonsLayout);
            newbtn.GetComponentInChildren<Text>().text = room.Name + "\n" + roomList[i].PlayerCount + " / " + roomList[i].MaxPlayers;
            newbtn.GetComponent<Button>().onClick.AddListener(() => PhotonNetwork.JoinRoom(room.Name));
            buttonObjects[i] = newbtn;
            ++i;
        }
    }

    private IEnumerator checkForAlivePlayers()
    {
        bool alive = false;
        while(true)
        {
            alive = false;
            foreach (var plr in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (plr.transform.position.y > -500.0f)
                    alive = true;
            }
            if(!alive)
            {
                // GAME OVER
                Debug.Log("Game over!");
                GameOverPopup.SetActive(true);
                GameOverText.text = "GAME OVER\nALL ZOMBIES DESTROYED";
                StartCoroutine(returnToMainIn(6f));
                yield return new WaitForSeconds(9f);
            }
            yield return new WaitForSeconds(0.2f);
        }
    }

    private IEnumerator returnToMainIn(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
}
