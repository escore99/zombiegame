﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetCharacterAttack : Photon.MonoBehaviour
{
    public Collider attackCol;
    public float treshold = 50;
    public float cooldown;
    public float specialCooldown;
    public float attackPwr;
    public float specialAttackPwr;
    public bool attackDisabled;
    //public PhotonView View;

    public List<NetEnemyHealth> enemies;
    
    public Animator Ani;
    //public NetAnimator NetAni;

    // Use this for initialization
    void Start()
    {
        enemies = new List<NetEnemyHealth>();
        StartCoroutine(removeNullEnemiesCORO());
    }

    // Update is called once per frame
    void Update()
    {

        if (photonView.isMine && Input.GetMouseButtonDown(0) && !attackDisabled)
        {
            photonView.RPC("NormalAttack", PhotonTargets.All);
            StartCoroutine(DisableAttack(cooldown));
            //Character attack animation
        }
        if (photonView.isMine && Input.GetButtonDown("Fire2") && !attackDisabled)
        {
            photonView.RPC("SpecialAttack", PhotonTargets.All);
            StartCoroutine(DisableAttack(specialCooldown));
        }
    }

    [PunRPC]
    public void NormalAttack()
    {
        //NetAni.PlayAnimation("AttackState");
        if (Ani != null)
            Ani.Play("AttackState", 0);
        if (PhotonNetwork.isMasterClient)
        {
            // Client ja master voi olla eri mieltä mitä enemeitä on triggerissä
            foreach (NetEnemyHealth eh in enemies)
            {
                if (eh == null) continue;
                //Debug.Log("Hit enemy");
                eh.TakeDmg(attackPwr);
            }
        }
    }

    [PunRPC]
    public void SpecialAttack()
    {
        if (Ani != null)
            Ani.Play("AttackState", 0);
        if (PhotonNetwork.isMasterClient)
        {
            foreach (NetEnemyHealth eh in enemies)
            {
                if (eh == null) continue;
                if (eh.health <= treshold)
                {
                    //Debug.Log("Hit enemy with special attack");
                    eh.TakeDmg(specialAttackPwr);
                }
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
     
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            NetEnemyHealth eh = other.GetComponent<NetEnemyHealth>();
            if (eh != null && !enemies.Contains(eh))
            {
                enemies.Add(eh);
                //Debug.Log("Enemy added");
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            NetEnemyHealth eh = other.GetComponent<NetEnemyHealth>();
            if (eh != null && enemies.IndexOf(eh) >= 0)
            {
                enemies.Remove(eh);
                //Debug.Log("Enemy removed");
            }
        }
    }

    IEnumerator DisableAttack(float time)
    {
        yield return new WaitForSeconds(0.01f);
        attackDisabled = true;
        //attackCol.enabled = false;
        yield return new WaitForSeconds(time);
        attackDisabled = false;
        //attackCol.enabled = true;
    }

    private IEnumerator removeNullEnemiesCORO()
    {
        while (true)
        {
            enemies.RemoveAll(item => item == null);
            yield return new WaitForSeconds(1f);
        }
    }
}
