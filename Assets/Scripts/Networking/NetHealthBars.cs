﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetHealthBars : Photon.PunBehaviour
{
    public GameObject HealthBarPrefab;
    public Transform HPBarLayout;

    //private List<HealthBar> healthBars;
    private Dictionary<int, HealthBar> barDict;

    // Use this for initialization
    void Start()
    {
        //healthBars = new List<HealthBar>();
        barDict = new Dictionary<int, HealthBar>();
        //healthBars.Add(HPBarLayout.GetComponentInChildren<HealthBar>());
    } 

    // Update is called once per frame
    void Update()
    {
        var photonViews = UnityEngine.Object.FindObjectsOfType<PhotonView>();
        foreach (var view in photonViews)
        {
            var player = view.owner;
            if (player != null)
            {
                if(barDict[player.ID] != null)
                {
                    NetPlayerHealth nethp = view.gameObject.GetComponent<NetPlayerHealth>();
                    if (nethp != null)
                        barDict[player.ID].SetFill(view.gameObject.GetComponent<NetPlayerHealth>().Fill);
                    //else
                    //    Debug.LogError("No NetPlayerHealth found");
                }
            }
        }

        //foreach (var plr in PhotonNetwork.playerList)
        //{
        //    barDict[plr.ID].SetFill(plr);
        //}
    }

    public override void OnJoinedRoom()
    {
        GameObject newhpbar = Instantiate(HealthBarPrefab, HPBarLayout);
        //healthBars.Add(newhpbar.GetComponent<HealthBar>());
        //healthBars[0].NameText.text = PhotonNetwork.player.ID + " - " + PhotonNetwork.playerName;
        //healthBars[0].SetPlayer(null, PhotonNetwork.player.ID);
        barDict[PhotonNetwork.player.ID] = newhpbar.GetComponent<HealthBar>();
        barDict[PhotonNetwork.player.ID].NameText.text = PhotonNetwork.player.ID + " - " + PhotonNetwork.playerName;
        int i = 1;
        foreach (var player in PhotonNetwork.otherPlayers)
        {
            newhpbar = Instantiate(HealthBarPrefab, HPBarLayout);
            //healthBars.Add(newhpbar.GetComponent<HealthBar>());
            //healthBars[i].NameText.text = player.ID.ToString() + " " + player.NickName;
            //healthBars[i].SetPlayer(null, player.ID);
            barDict[player.ID] = newhpbar.GetComponent<HealthBar>();
            barDict[player.ID].NameText.text = player.ID.ToString() + " - " + player.NickName;
            ++i;
        }

        //int i = 0;
        //var photonViews = UnityEngine.Object.FindObjectsOfType<PhotonView>();
        //foreach (var view in photonViews)
        //{
        //    var player = view.owner;
        //    //Objects in the scene don't have an owner, its means view.owner will be null
        //    if (player != null)
        //    {
        //        GameObject newhpbar = Instantiate(HealthBarPrefab, HPBarLayout);
        //        healthBars.Add(newhpbar.GetComponent<HealthBar>());
        //        healthBars[i].NameText.text = player.NickName;
        //        healthBars[i].SetPlayer(view.gameObject.GetComponent<NetPlayerHealth>(), player.ID);
        //        //var playerPrefabObject = view.gameObject;
        //        //do works...
        //    }
        //}


        //PhotonNetwork.playerName
        //PhotonNetwork.playerList
    }

    // Called when a remote player entered the room. This PhotonPlayer is already added to the playerlist at this time.
    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        GameObject newhpbar = Instantiate(HealthBarPrefab, HPBarLayout);
        //healthBars.Add(newhpbar.GetComponent<HealthBar>());
        //healthBars[healthBars.Count - 1].NameText.text = newPlayer.ID.ToString() + " " + newPlayer.NickName;
        //healthBars[healthBars.Count - 1].SetPlayer(null, newPlayer.ID);
        barDict[newPlayer.ID] = newhpbar.GetComponent<HealthBar>();
        barDict[newPlayer.ID].NameText.text = newPlayer.ID.ToString() + " - " + newPlayer.NickName;
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        Destroy(barDict[otherPlayer.ID].gameObject);
        //for (int i = 0; i < healthBars.Count; i++)
        //{
        //    if(healthBars[i].ActorID == otherPlayer.ID)
        //    {
        //        Destroy(healthBars[i].gameObject);
        //        healthBars.RemoveAt(i);
        //        return;
        //    }
        //}
    }

}
