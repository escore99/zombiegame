﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetPowerupManager : Photon.PunBehaviour
{
    //public GameObject powerUp;
    public Transform[] powerUpSpawnPoints;
    public bool spawnPowerUp;
    public float powerUpCooldown;

    private bool inRoom = false;

    // Use this for initialization
    void Start()
    {
        //InvokeRepeating("Spawn", powerUpCooldown, powerUpCooldown);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Spawn()
    {
        if(PhotonNetwork.isMasterClient && inRoom)
        {
            int spawnPointIndex = Random.Range(0, powerUpSpawnPoints.Length);
            PhotonNetwork.Instantiate("NetPowerup", powerUpSpawnPoints[spawnPointIndex].position, Quaternion.identity, 0);
            //Instantiate(powerUp, powerUpSpawnPoints[spawnPointIndex].position, powerUpSpawnPoints[spawnPointIndex].rotation);
        }
    }

    public override void OnJoinedRoom()
    {
        inRoom = true;
        InvokeRepeating("Spawn", powerUpCooldown, powerUpCooldown);
    }

    public override void OnLeftRoom()
    {
        inRoom = false;
        CancelInvoke();
    }
}