﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO add methods/RPC for more than movement
public class NetworkPlayer : Photon.MonoBehaviour
{
    private Vector3 realPosition;
    private Quaternion realRotation;

    // Use this for initialization
    void Start()
    {
        realPosition = transform.position;
        // TODO different script for enemies?
        if(gameObject.CompareTag("Enemy"))
        {
            if (!PhotonNetwork.isMasterClient)
            {
                // TODO AI:n liikkumisen synkkaus
                //GetComponent<StateController>().enabled = false;
                // TODO when master leaves, enable this for new master
            }
        }
        else if(!photonView.isMine)
        {
            GetComponent<CharacterMovement>().enabled = false;
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, realPosition, 0.2f);
            transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 0.2f);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // Our player. Send position to network
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            // Someone else's player. We receive this position info
            realPosition = (Vector3)stream.ReceiveNext();
            realRotation = (Quaternion)stream.ReceiveNext();
        }
    }
}