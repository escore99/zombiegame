﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetMoveTest : Photon.MonoBehaviour
{
    public Text DebugText;
    public GameObject NetRock;

	// Use this for initialization
	void Start () {
        //DebugText = FindObjectOfType<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * 5f * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * 5f * Time.deltaTime);
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            float[] colors = new float[3];
            colors[0] = Random.Range(0f, 1f);
            colors[1] = Random.Range(0f, 1f);
            colors[2] = Random.Range(0f, 1f);
            photonView.RPC("NewColor", PhotonTargets.All, colors);
        }

        if(Input.GetKeyDown(KeyCode.F))
        {
            // Start position, rotation, force
            // Should have a rock start point infront of player to avoid colliding with player collider
            // Rock should have script to handle collisions
            object[] prms = new object[3];
            prms[0] = (object)transform.position;
            prms[1] = (object)transform.rotation;
            prms[2] = (object)10f;
            photonView.RPC("ThrowRock", PhotonTargets.All, prms);
        }

    }

    [PunRPC]
    public void NewColor(float[] colors)
    {
        Color newColor = new Color(colors[0], colors[1], colors[2]);
        GetComponent<MeshRenderer>().material.color = newColor;
    }

    [PunRPC]
    public void ThrowRock(Vector3 sp, Quaternion rot, float pwr)
    {
        GameObject rock = (GameObject)Instantiate(NetRock, sp, rot);
        rock.GetComponent<Rigidbody>().AddForce(rock.transform.forward * pwr, ForceMode.Impulse);
        //currentShell.tag = "Shell";
        Destroy(rock, 10f);
    }

}
