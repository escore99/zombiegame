﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetEnemySpawner : Photon.MonoBehaviour
{
    public Transform[] EnemySpawnPoints;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.P))
        {
            SpawnEnemy();
        }
	}
    
    // Different types?
    public void SpawnEnemy()
    {
        if(PhotonNetwork.isMasterClient)
        {
            int spawnpoint = Random.Range(0, EnemySpawnPoints.Length);
            PhotonNetwork.InstantiateSceneObject("NetEnemy", EnemySpawnPoints[spawnpoint].position, EnemySpawnPoints[spawnpoint].rotation, 0, null);
        }
    }
}
