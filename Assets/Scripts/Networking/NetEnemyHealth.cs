﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetEnemyHealth : Photon.MonoBehaviour {

    public float health;
    public float maxHealth;
    public Image healthBar;
    public ParticleSystem blood;
    public GameObject bigBlood;

    public SoundManager sound;
    public GameObject AudioPrefab;
    CharacterAttack ca;

    private NetGameController gameController;
    private AudioSource soundSource;

    // Use this for initialization
    void Start()
    {
        gameController = FindObjectOfType<NetGameController>();
        soundSource = GetComponent<AudioSource>();
        health = 100;
        maxHealth = 100;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DestroySelf()
    {
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.Destroy(gameObject);
        }
    }
    
    public void TakeDmg(float dmg)
    {
        if (PhotonNetwork.isMasterClient)
        {
            dmg += Random.Range(0, 10);
            photonView.RPC("NetTakeDamage", PhotonTargets.All, dmg);
        }
    }

    [PunRPC]
    public void NetTakeDamage(float dmg)
    {
        Debug.Log("Damage: " + dmg);
        health -= dmg;

        blood.Play();
        
        soundSource.PlayOneShot(sound.GetSound("enemyHit"), 1f);

        healthBar.fillAmount = health / maxHealth;
        if ((health / maxHealth) <= 0.5)
            healthBar.color = new Color32(255, 255, 0, 100);
        if ((health / maxHealth) <= 0.2)
            healthBar.color = new Color32(255, 0, 0, 100);
        //Debug.Log("Enemy takes damage");
        if (health <= 0)
        {
            if (GetComponent<StateController>().HasFood)
            {
                if (GameData.Instance.NetworkMultiplayer)
                {
                    FindObjectOfType<NetFoodRemaining>().RestoreFood(10f, transform.position);
                }
                else
                {
                    FindObjectOfType<FoodRemaining>().RestoreFood(10f, transform.position);
                }
            }
            //Debug.Log("Enemy dies");
            GameObject bBlood = (GameObject)Instantiate(bigBlood, transform.position, transform.rotation);
            Destroy(bBlood, 1f);
            GameObject a = (GameObject)Instantiate(AudioPrefab, transform.position, transform.rotation);
            a.GetComponent<AudioSource>().PlayOneShot(sound.GetSound("enemyDeath"), 1f);
            Destroy(a, 4f);
            //soundSource.PlayOneShot(sound.GetSound("enemyDeath"), 1f);
            if (photonView.isMine)
            {
                foreach (var hp in FindObjectsOfType<NetPlayerHealth>())
                {
                    hp.Heal(15);
                }
            }

            if (PhotonNetwork.isMasterClient)
            {
                gameController.enemiesRemaining--;
                gameController.UpdateStatus();
                PhotonNetwork.Destroy(gameObject);
            }
        }
    }
}
