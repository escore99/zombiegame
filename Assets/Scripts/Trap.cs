﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour {
    EnemyHealth enemyHealth;
    PlayerHealth playerHealth;
    private int damage;
	// Use this for initialization
	void Start () {
        damage = 25;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            enemyHealth = other.GetComponent<EnemyHealth>();
            enemyHealth.TakeDmg(damage);
            gameObject.SetActive(false);
        }
        if (other.tag == "Player")
        {
            playerHealth = other.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
            gameObject.SetActive(false);
        }
    }
   
}
