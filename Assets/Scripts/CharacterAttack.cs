﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterAttack : MonoBehaviour {
    //EnemyHealth ehp;
    public Collider attackCol;
    public float treshold = 50;
    public float cooldown;
    public float specialCooldown;
    public float attackPwr;
    public float specialAttackPwr;
    //public Text text;
    public bool attackDisabled;
    public PlayerHealth ph;

    public List<EnemyHealth> enemies;

    public Animator Ani;

    // Use this for initialization
    void Start () {
        enemies = new List<EnemyHealth>();
        StartCoroutine(removeNullEnemiesCORO());
        //if (text == null)
        //    text = GameObject.Find("TestAttackText").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0) && !attackDisabled)
        {
            if (Ani != null)
                Ani.Play("AttackState", 0);
            foreach (EnemyHealth eh in enemies)
            {
                if (eh == null) continue;
                Debug.Log("Hit enemy");
                eh.TakeDmg(attackPwr);
            }
            StartCoroutine(DisableAttack(cooldown));
            //Character attack animation
        }
        if (Input.GetButtonDown("Fire2") && !attackDisabled)
        {
            if (Ani != null)
                Ani.Play("AttackState", 0);
            foreach (EnemyHealth eh in enemies)
            {
                if (eh == null) continue;
                if(eh.health <= treshold) // Onko kaikken vihujen treshold sama?
                {
                    ph.RegenHealth();
                    GameObject bBlood = (GameObject)Instantiate(eh.bigBlood, transform.position, transform.rotation);
                    //GameObject dyingSound = (SoundManager)Instantiate(eh.dying.GetSound("enemyHit"), transform.position, transform.rotation);
                    Destroy(bBlood, 1f);
                    Debug.Log("Hit enemy with special attack");
                    eh.TakeDmg(specialAttackPwr, this);
                }
            }
            StartCoroutine(DisableAttack(specialCooldown));
        }
    }
    
    void OnTriggerStay(Collider other)
    {
        // Tän vois tehdä fiksummin
        if (other.CompareTag("Enemy") && other.GetComponent<EnemyHealth>().health <= treshold)
        {
            //text.enabled = true;
        }
        //else text.enabled = false;
        /*
            //Inform player about special attack
            //Debug.Log(message);
            if (Input.GetButtonDown("Fire2") && !attackDisabled)
            {
                StartCoroutine(DisableAttack(specialCooldown));
                Debug.Log("Hit enemy with special attack");
                ehp = other.GetComponent<EnemyHealth>();
                ehp.TakeDmg(specialAttackPwr, this); //gameObject.GetComponent<CharacterAttack>()
                //StartCoroutine(DisableAttack(cooldown));
            }

        }
        if (other.CompareTag("Enemy") && Input.GetMouseButtonDown(0) && !attackDisabled)
        {
            StartCoroutine(DisableAttack(cooldown));
            //Debug.Log("Hit enemy");
            ehp = other.GetComponent<EnemyHealth>();
            ehp.TakeDmg(attackPwr);
            Debug.Log("Normal attack");
            //StartCoroutine(DisableAttack(cooldown));
        }
        */
    }
    
    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            EnemyHealth eh = other.GetComponent<EnemyHealth>();
            if(eh != null && !enemies.Contains(eh))
            {
                enemies.Add(eh);
                Debug.Log("Enemy added");
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            EnemyHealth eh = other.GetComponent<EnemyHealth>();
            if (eh != null && enemies.IndexOf(eh) >= 0)
            {
                enemies.Remove(eh);
                Debug.Log("Enemy removed");
            }
        }
    }

    IEnumerator DisableAttack(float time)
    {
        yield return new WaitForSeconds(0.01f);
        attackDisabled = true;
        //attackCol.enabled = false;
        yield return new WaitForSeconds(time);
        attackDisabled = false;
        //attackCol.enabled = true;
    }

    private IEnumerator removeNullEnemiesCORO()
    {
        while (true)
        {
            enemies.RemoveAll(item => item == null);
            yield return new WaitForSeconds(1f);
        }
    }
}
