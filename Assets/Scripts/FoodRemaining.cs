﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FoodRemaining : MonoBehaviour
{

    public Transform enemy;
    public Text food;

    public float foodAmount;

	// Use this for initialization
	void Start ()
	{
	    foodAmount = 100f;
	}
	
	// Update is called once per frame
	void Update () {
	   
    }

    public void TakeFood(float amount)
    {
        if (GameData.Instance.NetworkMultiplayer)
        {
            FindObjectOfType<NetFoodRemaining>().TakeFood(amount);
        }
        else
        {
            foodAmount -= amount;
        }
    }

    public void RestoreFood(float amount, Vector3 lastPos)
    {
        float dist = Vector3.Distance(gameObject.transform.position, lastPos);
        foodAmount += 1 / dist * amount;
    }

    public void ShowFoodAmount()
    {
        if (foodAmount <= 75f || foodAmount > 50f)
        {
            food.text = "Food remains: 75%";
        }
        if (foodAmount <= 50f || foodAmount > 25f)
        {
            food.text = "Food remains: 50%";
        }
        if (foodAmount <= 25f || foodAmount > 0f)
        {
            food.text = "Food remains: 25% Hurry up!";
        }
        if (foodAmount <= 0f)
        {
            food.text = "Food remains: 0%";
        }
    }

}
