﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public Image FillImage;
    public Text NameText;

    public NetPlayerHealth health;
    public int actorID = -99999;

    public int ActorID
    {
        get { return actorID; }
    }

    private void Update()
    {
        //if (health == null)
        //    return;

        //FillImage.fillAmount = health.Fill;
    }

    public void SetFill(float fill)
    {
        if (FillImage == null)
            return;
        FillImage.fillAmount = fill;
    }

    public void SetPlayer(NetPlayerHealth hp, int id)
    {
        //Debug.Log(hp);
        health = hp;
        actorID = id;
    } 
}