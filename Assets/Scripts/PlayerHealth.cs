﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    public int health;
    public Image healthBar;
    public float healthLossCooldown;
    public ParticleSystem blood;
    public SoundManager sm;
    public PlayerManager pm;
    public AudioSource hitSource;
    public AudioSource deathSource;

    private int maxHealth;
    private bool healthLossDisabled;
    private AudioClip hitSound;
    private AudioClip deathSound;

    void Awake()
    {
        hitSource = GetComponent<AudioSource>();
        deathSource = GetComponent<AudioSource>();
    }
    // Use this for initialization
    void Start ()
    {
        pm = FindObjectOfType<PlayerManager>();
        health = 100;
        maxHealth = 100;
        if(healthBar == null)
        {
            healthBar = GameObject.Find("PlayerHealth").GetComponent<Image>();
        }

	}

    public void TakeDamage(int amount)
    {
        if (!healthLossDisabled)
        {
            blood.Play();
            hitSound = sm.GetSound("playerHit");
            hitSource.PlayOneShot(hitSound, 1f);
            if (healthBar != null)
            {
                healthBar.fillAmount = (float)health / (float)maxHealth;
            }
            if(health>0)
            {
                health -= amount;
            }
            if (health <= 0)
            {
                deathSound = sm.GetSound("playerDeath");
                deathSource.PlayOneShot(deathSound, 1f);
                DisableHealthLoss(1f);
                FindObjectOfType<GameController>().SetOverviewCamera(true);
                Camera[] firstList = GameObject.FindObjectsOfType<Camera>();
                string nameToLookFor = "OverviewCamera";

                for (var i = 0; i < firstList.Length; i++)
                {
                    if (firstList[i].gameObject.name == nameToLookFor)
                    {
                        firstList[i].enabled = true;
                        Debug.Log("Camera found");
                    }
                }

                StartCoroutine(WaitDeathSound(deathSound));
                //TODO Respawn
                pm.RespawnPlayer(gameObject);
                health = 100;
                healthBar.fillAmount = (float)health / (float)maxHealth;
                //return;
                // Tässä pitäisi olla kuolemisanimaatio ja sen jälkeen playerille tulee game over tai spectate ruutu. Disabled poistaa myös kameran.
            }
        }
        StartCoroutine(DisableHealthLoss(healthLossCooldown));
    }

    public void RegenHealth()
    {
        Debug.Log("regen health");
        if ((maxHealth - health) < 15)
        {
            health = maxHealth;
        }
        else
        {
            health += 15;
        }
        healthBar.fillAmount = (float)health / (float)maxHealth;
    }

    IEnumerator DisableHealthLoss(float time)
    {
        yield return new WaitForSeconds(0.01f);
        healthLossDisabled = true;
        yield return new WaitForSeconds(time);
        healthLossDisabled = false;
    }

    IEnumerator WaitDeathSound(AudioClip clip)
    {
        yield return new WaitForSeconds(clip.length);

       // Destroy(gameObject);
        gameObject.SetActive(false);
    }
}
