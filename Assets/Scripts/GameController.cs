﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameController : MonoBehaviour {

    public GameObject enemy;
    public Transform[] enemySpawnPoints1;
    public Transform[] enemySpawnPoints2;
    public Transform[] enemySpawnPoints3;
    public bool spawnEnemy;
    public float enemyCooldown = 0;
    public Text nextWave;
    public Text status;
    public int enemiesRemaining = 3;
    public SoundManager sm;
    public Camera OverviewCamera;
    public Terrain Map;
    public GameObject EndedFolder;

    private bool firstRound = true;
    private int level = 1;
    private int enemyCounter = 0;
    private Transform[] currentSpawnPoints;
    private FoodRemaining foodRemaining;

    void Start()
    {
        foodRemaining = FindObjectOfType<FoodRemaining>();
        status.supportRichText = true;
        currentSpawnPoints = enemySpawnPoints1;
        StartCoroutine(NextWave(5));
        EndedFolder.gameObject.SetActive(false);
    }

    void Update()
    {
        if (enemyCounter >= 0 && spawnEnemy)
        {
            SpawnEnemy();
            StartCoroutine(DisableSpawn(enemyCooldown));
        }
        
    }
    void SpawnEnemy()
    {
        int spawnPoint = Random.Range(0, currentSpawnPoints.Length);
        Vector3 spawnpos = currentSpawnPoints[spawnPoint].position;
        if (Map != null)
        {
            spawnpos.y = Map.SampleHeight(spawnpos);
        }
        Debug.Log(spawnpos);
        Instantiate(enemy, spawnpos, currentSpawnPoints[spawnPoint].rotation);
        enemyCounter--;
        enemiesRemaining++;
        Debug.Log("Enemy spawned");
        UpdateStatus();

    }
    public void GameOver()
    {
        EndedFolder.gameObject.SetActive(true);
        Time.timeScale = 0;

    }

    IEnumerator DisableSpawn(float time)
    {
        spawnEnemy = false;
        yield return new WaitForSeconds(time);
        spawnEnemy = true;
    }

    IEnumerator NextWave(int time)
    {
        int min, sec;
       
        while (time >= 0)
        {
            sec = time % 60;
            min = (time - sec)/60;
           // Debug.Log(min + ":" + sec);
            if (firstRound)
            {
                if (sec < 10)
                {
                    nextWave.text = "First round in: \n" + min + ":0" + sec;
                }
                else
                {
                    nextWave.text = "First round in: \n" + min + ":" + sec;
                }
            }
            else
            {
                if (sec < 10)
                {
                    nextWave.text = "Next wave in: \n" + min + ":0" + sec;
                }
                else
                {
                    nextWave.text = "Next wave in: \n" + min + ":" + sec;
                }
            }
            yield return new WaitForSeconds(1);
            time -= 1;
        }

        if (firstRound)
        {
            firstRound = false;
        }
        else
        {
            level++;
        }

        if (level < 3)
        {
            currentSpawnPoints = enemySpawnPoints1;
        }
        else if (level < 5)
        {
            currentSpawnPoints = enemySpawnPoints2;
        }
        else if (level >= 5)
        {
            currentSpawnPoints = enemySpawnPoints3; 
        }
        spawnEnemy = true;
        enemyCounter = 10 + level*2;
        StartCoroutine(WaveRemaining(enemyCounter * enemyCooldown));
    }

    IEnumerator WaveRemaining(float time)
    {
        float min, sec;

        spawnEnemy = true;

        while (time >= 0)
        {
            sec = time % 60;
            min = (time - sec) / 60;
            if (sec < 10)
            {
                nextWave.text = "Wave remaining: \n" + min + ":0" + sec;
            }
            else
            {
                nextWave.text = "Wave remaining: \n" + min + ":" + sec;
            }
            yield return new WaitForSeconds(1);
            time -= 1;
        }

        StartCoroutine(NextWave(35));
    }

    public void UpdateStatus()
    {
        status.text = "<B>Level: " + level  + "\n</B><size= 14>Humans: " + enemiesRemaining +"\nFood remaining: " + foodRemaining.foodAmount.ToString("n0") + "</size>";
    }

    public void SetOverviewCamera(bool active)
    {
        if(OverviewCamera != null)
        {
            OverviewCamera.gameObject.SetActive(true);
        }
    }
}
