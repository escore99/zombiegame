﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    //public AudioSource source;
    public AudioClip[] sounds;

    //void Awake()
    //{
    //    Debug.Log("SoundManager awake");
    //    source = gameObject.GetComponent<AudioSource>();
    //}

    public AudioClip GetSound(string sound)
    {
        switch (sound)
        {
            case "enemyHit"://
                return sounds[0];
            case "enemyDeath"://
                return sounds[1];
            case "playerHit"://
                return sounds[2];
            case "playerDeath"://
                return sounds[3];
            case "setTrap":
                return sounds[4];
            case "springTrap"://
                return sounds[5];
            case "fallToGround":
                return sounds[6];
            case "jump"://
                return sounds[7];
            case "playerBreathing"://
                return sounds[8];
            case "enemyBreathing"://
                return sounds[9];
            case "enemyAfraid"://
                return sounds[10];
            case "footSteps"://
                return sounds[11];
            case "enemyPhrases"://
                return sounds[12];
            case "BGMusic"://
                return sounds[13];
            default:
                return null;

        }
    }
}
