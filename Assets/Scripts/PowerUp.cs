﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    public bool enemyTookPowerUp = false;

    public float speedMultiplier = 0;
    public float stunDuration = 0;
    public bool activated = false;
    public float powerUpLength = 0;
    // spotID = spawn location for powerup manager 
    public int spotID = 0;
    private float lifeTime = 15;
    public AudioSource trapSource;
    public SoundManager sm;

    private PowerUpManager powerUpManager;
    //private StateController controller;
    private AudioClip springTrapSound;

    void Awake()
    {
        trapSource = GetComponent<AudioSource>();
        
    }

    // Use this for initialization
    void Start ()
	{
	    powerUpManager = FindObjectOfType<PowerUpManager>();
	}
	
	// Update is called once per frame
	void Update () {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0 && !GameData.Instance.NetworkMultiplayer)
        {
            powerUpManager.spotUsed[spotID] = false;
            gameObject.SetActive(false);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            springTrapSound = sm.GetSound("springTrap");
            trapSource.PlayOneShot(springTrapSound, 1f);
            other.GetComponent<CharacterMovement>().StartCoroutine(other.GetComponent<CharacterMovement>().UsePowerUp(speedMultiplier, stunDuration, powerUpLength));
            StartCoroutine(WaitTrap(springTrapSound));
        }

        else if (other.tag == "Enemy")
        {
            springTrapSound = sm.GetSound("springTrap");
            trapSource.PlayOneShot(springTrapSound, 1f);
            other.GetComponent<StateController>().StartCoroutine(other.GetComponent<StateController>().UsePowerUp(speedMultiplier, stunDuration, powerUpLength));
            enemyTookPowerUp = true;
            if(!GameData.Instance.NetworkMultiplayer)
                powerUpManager.spotUsed[spotID] = false;
            StartCoroutine(WaitTrap(springTrapSound));
        }
    }

    IEnumerator WaitTrap(AudioClip clip)
    {
        yield return new WaitForSeconds(clip.length);
        gameObject.SetActive(false);
    }

}
