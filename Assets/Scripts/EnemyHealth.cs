﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{

    public float health;
    public float maxHealth;
    public Image healthBar;
    public ParticleSystem blood;
    public GameObject bigBlood;
    public AudioSource hitSource;
    public AudioSource dyingSource;

    public SoundManager sm;
    public GameObject AudioPrefab;
    CharacterAttack ca;

    private GameController gameController;
    private Vector3 lastPos;
    private AudioClip hitSound;
    private AudioClip dyingSound;

    void Awake()
    {
        hitSource = GetComponent<AudioSource>();
        dyingSource = GetComponent<AudioSource>();
    }
    // Use this for initialization
    void Start()
    {
        gameController = FindObjectOfType<GameController>();
        health = 100;
        maxHealth = 100;
    }

    public void TakeDmg(float dmg, CharacterAttack ca = null)
    {
        health -= dmg;
        blood.Play();
        hitSound = sm.GetSound("enemyHit");
        hitSource.PlayOneShot(hitSound, 0.3f);
        healthBar.fillAmount = health / maxHealth;
        if ((health / maxHealth) <= 0.5)
            healthBar.color = new Color32(255, 255, 0, 100);
        if ((health / maxHealth) <= 0.2)
            healthBar.color = new Color32(255, 0, 0, 100);
        Debug.Log("Enemy takes damage");
        if (health <= 0)
        {
            if (ca != null)
            {
                Debug.Log("Text on");
                //ca.text.enabled = false;
            }
            Debug.Log("Enemy dies");
            lastPos = gameObject.transform.position;
            gameController.enemiesRemaining--;
            gameController.UpdateStatus();
            //dyingSound = sm.GetSound("enemyDeath");
            //dyingSource.PlayOneShot(dyingSound, 0.10f);
            if (AudioPrefab != null)
            {
                GameObject a = (GameObject)Instantiate(AudioPrefab, transform.position, transform.rotation);
                a.GetComponent<AudioSource>().PlayOneShot(sm.GetSound("enemyDeath"), 1f);
                Destroy(a, 4f);
            }

            //GameObject death = (GameObject)Instantiate(dying, transform.position, transform.rotation);
            //Destroy(death, 1f);
            Destroy(gameObject);
        }

    }
}
