﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    public float cameraAngle;
    public bool goBack;
    public bool reverse;
    public float timer;
    public float lerp = 0.5f;
    public float duration = 10f;

    // Use this for initialization
    void Start()
    {
        timer = 0;
        goBack = false;
        reverse = false;
    }

    void CheckRotation()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if (cameraAngle < 90)
            {
                cameraAngle += 2;
            }
            goBack = true;
            timer = 0;
            reverse = false;

        }
        if (Input.GetKey(KeyCode.D))
        {
            if (cameraAngle > -90)
            {
                cameraAngle -= 2;
            }
            goBack = true;
            timer = 0;
            reverse = false;
        }
    }
    void UpdateRotation()
    {

        transform.localRotation = Quaternion.Euler(0, cameraAngle, 0);
    }

    void LerpBack()
    {
        
        transform.localRotation = Quaternion.Lerp(
       transform.localRotation, Quaternion.Euler(0, 0, 0),
       Time.deltaTime * lerp);
        if (transform.localRotation.eulerAngles.y > 100)
        {
            cameraAngle = transform.localRotation.eulerAngles.y - 360;
        }
        else
        {
            cameraAngle = transform.localRotation.eulerAngles.y;
        }
        if (transform.localRotation.eulerAngles.y > -359.5f && transform.localRotation.eulerAngles.y < 0.5f)
        {

            transform.localRotation = Quaternion.Euler(0, 0, 0);
            cameraAngle = 0;
            reverse = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckRotation();
        if (goBack)
        {
            timer += Time.deltaTime * 1f;
            UpdateRotation();

        }
        if (timer > 0.5f)
        {

            goBack = false;
            reverse = true;
            timer = 0;

        }
        if (reverse)
        {
            LerpBack();
        }

    }
}
