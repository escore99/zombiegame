﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput.PlatformSpecific;

public class CameraScript : MonoBehaviour {

    // Use this for initialization
    [Header("Health Settings")]
    public Rigidbody m_rigid;
    public CharacterController m_Char;
    [Header("Health Settings")]
    public Transform startMarker;
    public Transform endMarker;
    public Transform curPos;
    public bool m_OnMove;
    public bool m_MoveCamera;
    public bool m_BoolHelp;
    public bool m_LerpNotComplete;
    public float m_Speed;
    [Range(0.0f, 10.0f)]
    public float m_LerpSpeed = 3.0F;
    public float startTime;
    public float m_LerpTime;
    public float m_JourneyLength;
    public float m_LerpLength;
    public GameObject m_Player;


    void Start ()
    {
        m_OnMove = false;
         startTime = Time.time;
        m_MoveCamera = false;
        m_JourneyLength = Vector3.Distance(startMarker.position, endMarker.position);
        m_BoolHelp = m_OnMove;
        m_LerpNotComplete = false;
        m_rigid = m_Player.GetComponent<Rigidbody>();
        m_Char = m_Player.GetComponent<CharacterController>();
        curPos = transform;
        m_LerpTime = 0;
    }

    // Update is called once per frame
    void Update ()
    {
      //  m_Speed = m_rigid.velocity.magnitude;
        m_Speed = m_Char.velocity.magnitude;


        if (m_Speed > 1)
        {
            m_OnMove = true;
        }
        else
        {
            m_OnMove = false;
        }


        
        if(m_OnMove != m_BoolHelp)
        {
            if (m_MoveCamera)
            {
                m_LerpNotComplete = true;
                curPos.position = transform.position;
                m_LerpLength =Vector3.Distance(curPos.position, endMarker.position);
            }
            else
            {
                m_LerpNotComplete = false;
            }
            m_BoolHelp = m_OnMove;
            m_MoveCamera = true;
            startTime = Time.time;
            m_LerpTime = 0;

        }
        if (!m_OnMove && m_MoveCamera)
        {
            //Kohti pelaajaa
            float distCovered = (Time.time - (startTime - m_LerpTime)) * m_LerpSpeed;
            float fracJourney = distCovered / m_JourneyLength;
            m_LerpTime += Time.deltaTime;
            if (m_LerpNotComplete)
            {
               
                float journey = ((Time.time - (startTime -m_LerpTime)) * (m_LerpSpeed*0.2f)) /
                                  m_LerpLength;
                transform.position = Vector3.Lerp(curPos.position, endMarker.position, journey);
            }
            else
            {
                transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fracJourney);
            }
            if(transform.position == endMarker.position)
            {
                startTime = Time.time;
                m_MoveCamera = false;
                m_LerpNotComplete = false;
                m_LerpTime = 0;
            }
        }
        if (m_OnMove && m_MoveCamera)
        {
            //Pois pelaajasta
            float distCovered = (Time.time - startTime) * m_LerpSpeed;
            float fracJourney = distCovered / m_JourneyLength;
            m_LerpTime += Time.deltaTime;
            if (m_LerpNotComplete)
            {
               

                float journey = ((Time.time - (startTime-m_LerpTime))* m_LerpSpeed) /
                                m_JourneyLength;

                transform.position = Vector3.Lerp(curPos.position, startMarker.position, journey);
            }
            else
            {
                transform.position = Vector3.Lerp(endMarker.position, startMarker.position, fracJourney);
            }
            if (transform.position == startMarker.position)
            {
                startTime = Time.time;
                m_MoveCamera = false;
                m_LerpNotComplete = false;
                m_LerpTime = 0;
                transform.position = startMarker.position;

            }
        }
        
    }
}
