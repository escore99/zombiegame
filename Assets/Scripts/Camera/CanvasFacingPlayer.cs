﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFacingPlayer : MonoBehaviour {

    private Camera m_Camera;

    // Use this for initialization
    void Start () {
        if(m_Camera == null)
            m_Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>() as Camera;
    }

    // Update is called once per frame
    
    void Update()
    {
        if (m_Camera != null)
        {
            transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
                m_Camera.transform.rotation * Vector3.up);
        }
    }
}
