﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDistanceController : MonoBehaviour {
    public RayCast3 raycast;
    public float distance = 1.5f;
    public float basicDistance = 1.5f;
    public Vector3 originalPosition;

    float x = 0.0f;
    float y = 0.0f;


    // Use this for initialization
    void Start () {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }
	
	// Update is called once per frame
	void Update () {
        distance = raycast.distance;
        if (distance < 1.5f)
        {

            Quaternion rotation = Quaternion.Euler(y, x, 0);
            Vector3 newDistance = new Vector3(0, 0, distance);
            newDistance = transform.localPosition;
            
            newDistance.z = -distance+0.1f;
            transform.localPosition = newDistance;

        }
        else
        {
            distance = basicDistance;
        }
    }
}
