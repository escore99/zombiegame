﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    private Rigidbody rigidBody;
    private Transform myTransform;
    public float moveSpeed = 5f;


    // Use this for initialization
    void Start() {
        rigidBody = GetComponent<Rigidbody>();
        myTransform = transform;
    }
    private void UpdatePlayer1Movement()
    {
        if (Input.GetKey(KeyCode.W))
        { //Up movement
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, moveSpeed);
           // myTransform.rotation = Quaternion.Euler(0, 0, 0);

        }

        if (Input.GetKey(KeyCode.Q))
        { //Left movement
            rigidBody.velocity = new Vector3(-moveSpeed, rigidBody.velocity.y, rigidBody.velocity.z);
            //myTransform.rotation = Quaternion.Euler(0, 270, 0);

        }

        if (Input.GetKey(KeyCode.S))
        { //Down movement
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, -moveSpeed);
           // myTransform.rotation = Quaternion.Euler(0, 180, 0);

        }

        if (Input.GetKey(KeyCode.E))
        { //Right movement
            rigidBody.velocity = new Vector3(moveSpeed, rigidBody.velocity.y, rigidBody.velocity.z);
           // myTransform.rotation = Quaternion.Euler(0, 90, 0);

        }
        if (Input.GetKey(KeyCode.A))
        { //Right movement
           myTransform.Rotate(Vector3.up * -moveSpeed *60* Time.deltaTime);
         

        }
        if (Input.GetKey(KeyCode.D))
        { //Right movement
            myTransform.Rotate(Vector3.up * moveSpeed * 60 * Time.deltaTime);

        }
    }

        // Update is called once per frame
        void Update() {
            UpdatePlayer1Movement();
        
    }
}
