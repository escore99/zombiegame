﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class MouseOrbitImproved : MonoBehaviour
{

    public RayCast3 raycast;
    public Transform target;
    public float distance = 2.0f;
    public float camDistance = 2.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    public Rigidbody rigidbody;
    public float m_speed;
    public float minDistance = 0.5f;
    public float maxDistance = 3f;
    public float lerp = 0f, duration = 10f;

    float x = 0.0f;
    float y = 0.0f;
  

    // Use this for initialization
    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        //rigidbody = GetComponent<Rigidbody>();

        // Make the rigid body not change rotation
        if (rigidbody != null)
        {
            rigidbody.freezeRotation = true;
        }
    }

    public void Update()
    {
        m_speed = rigidbody.velocity.magnitude;
        distance = raycast.distance;
        if (distance > 2)
        {
         //   camDistance = 2;
        }
        if ( m_speed <1.6f){
            if (camDistance > 2)
            {


                camDistance = m_speed;
                //camDistance = 2;
                Debug.Log(Mathf.Lerp(camDistance, 2, lerp));
               
            }
            else
            {
                lerp = 0;
            }

        }
      
        else if(m_speed >= 1.6f)

        {
            
            if (camDistance < 3)
            {
               
                camDistance = m_speed;
                
            }
            else
            {
                lerp = 0;
                
            }
           
            
        }
        if (distance < 2)
        {
            camDistance = distance;
        }
        if ((Input.GetKey(KeyCode.A)) || (Input.GetKey(KeyCode.LeftArrow)))
        {
            x -= 3; //The higher the number the faster the camera rotation
        }
        if ((Input.GetKey(KeyCode.D)) || (Input.GetKey(KeyCode.RightArrow)))
        {
            x += 3; //The higher the number the faster the camera rotation
        }
    }

    void LateUpdate()
    {
        if (target)
        {
           // x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
           // y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);

           // distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

            RaycastHit hit;
            if (Physics.Linecast(target.position, transform.position, out hit))
            {
                //distance -= hit.distance;
            }
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -camDistance);
            Vector3 position = rotation * negDistance + target.position;

            transform.rotation = rotation;
            transform.position = position;
            if (m_speed <0.1f)
            {
                
            }
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}