﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public Transform Target;    //The target is what the camera will look at

    private Vector3 _cameraMovementVector;  //The camera input from the controller

    private Vector3 _offset;    //The difference between the camera's
                                //position and its "target"

    void Start()
    {
        //Initialize our offset to put the camera at the position we want.
        //Through trial and error, I found that these values worked nicely
        //and put the camera in a good spot
        this._offset = new Vector3(this.Target.position.x - 130.0f,
                    this.Target.position.y + 15.0f, this.Target.position.z - 105.0f);
    }

    //Use LateUpdate to ensure that the player's position actually changed (in update)
    //before moving the camera to follow it
    void LateUpdate()
    {
        //Check for camera movement input
        this._cameraMovementVector.x = Input.GetAxis("Horizontal");
        this._cameraMovementVector.y = Input.GetAxis("Vertical");

        //Adjust the camera's offset based on our input (for rotation)
        this._offset = Quaternion.AngleAxis(this._cameraMovementVector.x, Vector3.up)
                    * this._offset;

        //Adjust the camera's possition based on our offset
        this.transform.position = this.Target.position + _offset;

        //Adjust the camera's rotation to "look at" its target
        this.transform.LookAt(new Vector3(this.Target.position.x,
                    this.transform.position.y, this.Target.position.z));
    }
}
