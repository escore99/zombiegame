﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCast3 : MonoBehaviour
{
    public float distance;
    public RaycastHit hit;
    // Use this for initialization
    void Start()
    {
        distance = 5;

    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
        {
            distance = hit.distance;
        }
    }
}
