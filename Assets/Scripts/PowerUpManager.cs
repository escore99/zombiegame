﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class PowerUpManager : MonoBehaviour {
    public GameObject powerUp;
    public Transform[] powerUpSpawnPoints = null;
    public float powerUpCooldown = 0;
    public bool[] spotUsed;

    // Use this for initialization
    void Start () {
        InvokeRepeating("Spawn", powerUpCooldown, powerUpCooldown);
        spotUsed = new bool[powerUpSpawnPoints.Length];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Spawn()
    {
        int spawnPointIndex = Random.Range(0, powerUpSpawnPoints.Length);
        // Checking if there is already a powerup spawned on the spot. If true keep changing spot until it's not taken.
        while (spotUsed[spawnPointIndex] == true)
        {
            spawnPointIndex = Random.Range(0, powerUpSpawnPoints.Length);
        }
        spotUsed[spawnPointIndex] = true;
        GameObject newObject = Instantiate(powerUp, powerUpSpawnPoints[spawnPointIndex].position, powerUpSpawnPoints[spawnPointIndex].rotation);
        PowerUp powerUpID = newObject.GetComponent<PowerUp>();
        powerUpID.spotID = spawnPointIndex;

    }
}
