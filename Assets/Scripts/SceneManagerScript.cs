﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneManagerScript : MonoBehaviour
{
    public GameObject m_MenuHolder;
    public bool m_MenuOpen;
    public bool m_MenuToggle;
    // Use this for initialization
    void Start()
    {
        m_MenuHolder.SetActive(false);
        m_MenuOpen = false;
        m_MenuToggle = false;
    }

    public void OpenMenu()
    {
        m_MenuToggle = false;

        m_MenuHolder.SetActive(true);
        m_MenuOpen = false;
        if (SceneManager.GetActiveScene().name == "MultiPlayer")
        {
            // Multiplayer scenessä ei voi pausettaa aikaa.
        }
        else
        {
            Time.timeScale = 0;
        }

    }
    public void CloseMenu()
    {
        m_MenuToggle = false;

        m_MenuHolder.SetActive(false);
        m_MenuOpen = false;
        if (SceneManager.GetActiveScene().name != "MultiPlayer")
        {
            // Multiplayer scenessä ei voi pausettaa aikaa.
        }
        else
        {
            Time.timeScale = 1;
        }

    }
    public void LoadNewScene(int sceneIndex)
    {
        if (SceneManager.GetActiveScene().name == "MultiPlayer")
        {

        }
        SceneManager.LoadScene(sceneIndex);

    }
    public void ExitGame()
    {

        Application.Quit();
    }
   
    // Update is called once per frame
    void Update()
    {
      

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            m_MenuOpen = !m_MenuOpen;
            m_MenuToggle = true;
        }

        if (m_MenuOpen&&m_MenuToggle)
        {
            m_MenuToggle = false;

            m_MenuHolder.SetActive(true);
            if (SceneManager.GetActiveScene().name != "MultiPlayer")
            {
                Time.timeScale = 0;
            }
            

        }
        else if (!m_MenuOpen && m_MenuToggle)
        {
            m_MenuToggle = false;

            m_MenuHolder.SetActive(false);
            if (SceneManager.GetActiveScene().name != "MultiPlayer")
            {
                Time.timeScale = 1;
            }


        }

    }

}
