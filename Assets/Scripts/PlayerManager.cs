﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public GameObject player;
    public GameObject playerBig;
    public GameObject activeZombie;
    public GameObject zombieSelect;

    public GameController gm;
    public Transform[] playerSpawnPoints;
    public int playerLives;
    public Camera SpectatorCam;
    public Camera OverviewCam;

    //asD
	// Use this for initialization
	void Start ()
    {
        SpawnPlayer();		
	}
    public void SelectZombie()
    {
        zombieSelect.gameObject.SetActive(true);
    }
    public void ChooseBig()
    {
        activeZombie = playerBig;
        zombieSelect.gameObject.SetActive(false);
        SpawnPlayer(); 

    }
    public void ChooseSmall()
    {
        activeZombie = player;
        zombieSelect.gameObject.SetActive(false);
        SpawnPlayer();
    }
    void Update ()
    {
		
	}

    private void SpawnPlayer()
    {
        int pos = Random.Range(0, 3);
        activeZombie = Instantiate(activeZombie, playerSpawnPoints[pos].position, Quaternion.identity);
        Debug.Log(activeZombie.GetInstanceID());
        if (SpectatorCam != null)
            SpectatorCam.enabled = false;
        if (OverviewCam != null)
            OverviewCam.enabled = false;
        FindObjectOfType<GameController>().SetOverviewCamera(false);
    }
  
    public void RespawnPlayer(GameObject player)
    {
        if (playerLives > 0)
        {
            playerLives -= 1;
           // int pos = Random.Range(0, 3);
           // activeZombie.transform.position = playerSpawnPoints[pos].position;
            SelectZombie();
            //player.SetActive(true);
        }
        else
        {
            gm.GameOver();
        }
    }
}
