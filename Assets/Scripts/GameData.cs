﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Saves game data during runtime
/// </summary>
public class GameData
{
    #region singleton
    private static GameData instance;

    private GameData() { }

    public static GameData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameData();
            }
            return instance;
        }
    }
    #endregion

    // what data do we need to save?
    #region private variables
    private bool localMultiplayer = false;
    private bool networkMultiplayer = false;
    #endregion

    #region properties
    public bool LocalMultiplayer
    {
        get { return localMultiplayer; }
        set { }
    }
    public bool NetworkMultiplayer
    {
        get { return networkMultiplayer; }
        set { }
    }
    #endregion

    // Set game mode method?
    public void SetGameMode(GameMode mode)
    {
        switch(mode)
        {
            case GameMode.Singleplayer:
            default:
                localMultiplayer = false;
                networkMultiplayer = false;
                break;
            case GameMode.LocalMultiplayer:
                localMultiplayer = true;
                networkMultiplayer = false;
                break;
            case GameMode.NetworkMultiplayer:
                localMultiplayer = false;
                networkMultiplayer = true;
                break;
        }
    }
}

public enum GameMode
{
    Singleplayer,
    LocalMultiplayer,
    NetworkMultiplayer,
}
