﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


using UnityEngine.Networking;

public class MyClass
{
    
    public string name;
}
public class ChatHandler : MonoBehaviour
{
    public string activeChannel;
    public InputField textSender;
    public InputField chooseChannel;

    public Text[] chatfield = new Text[10];
    MyClass myObject = new MyClass();

    void Start()
    {
       
      
        myObject.name = "Dr Charles Francis";
         StartCoroutine(GetText());
       // StartCoroutine(SendText("Pertti"));
    }

    IEnumerator SendText(string textToSend)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", textToSend);
        //"name":"Reijo"
       
        string json = JsonUtility.ToJson(myObject);
        Debug.Log(json);

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost:5000/api/chat/",json))
        {
            Debug.Log(www);
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
    }
    IEnumerator UpdateChat()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost:5000/api/players/"))
        {
            yield return www.Send();
        }
    }
    IEnumerator GetChanels()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost:5000/api/players/"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);

                // Or retrieve results as binary data
                //  byte[] results = www.downloadHandler.data;

            }
        }
    }
    IEnumerator GetText()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost:5000/api/chat/"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);

                // Or retrieve results as binary data
              //  byte[] results = www.downloadHandler.data;
            
            }
        }
    }
    public void RenderChatToScreen()
    {

    }
    public void SetChannel(string channelName)
    {
        activeChannel = channelName;
    }
}