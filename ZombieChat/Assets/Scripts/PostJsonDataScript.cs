﻿using UnityEngine;
using System.Collections;
using System.Text;
using System;
using UnityEngine.UI;


public class PostJsonDataScript : MonoBehaviour
{
    // Use this for initialization
   private readonly string _baseurl = "http://localhost:5000/api/chat/";
    string _url;
    [Header("Basic Info")]
    public string _channel;
    public string _playerName;

    [Header("UI")]
    public Button activateChannel;
    public InputField inputText;
    public InputField inputChannel;
    public InputField inputPlayerName;
    public GameObject container;
    [Header("Chat")]
    public Text[] _chatfield = new Text[10];
    public Chat _chat;
    private float time;

    void Start()
    {
        for (int i = 0; i < _chatfield.Length; i++)
        {
            _chatfield[i].text = "";
            
        }
       
        _url = "http://localhost:5000/api/chat/";
        _url = _url + _channel;
        container.SetActive(false);
        inputChannel.gameObject.SetActive(false);
        time = 0;

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            inputText.ActivateInputField();
        }
        time = time + Time.deltaTime;
        if (time >1 && container.activeInHierarchy )
        {
            RefreshChat();
            time = 0;
        }

    }
    public void SetName()
    {
        if (inputPlayerName.text != "")
        {
            _playerName = inputPlayerName.text;
        }
        inputPlayerName.gameObject.SetActive(false);
        inputChannel.gameObject.SetActive(true);
        inputChannel.ActivateInputField();
    }
        public void ToggleChannel()
    {
        if (container.activeInHierarchy)
        {
            container.SetActive(false);
            inputChannel.gameObject.SetActive(true);
        }
        else
        {
            container.SetActive(true);
            inputChannel.gameObject.SetActive(false);
        }
    }
   
    public void RenderChatToScreen()
    {
        if (_chat.messages.Length > 0)
        {
            int t = 0;
            for (int i = _chat.messages.Length-1; i >=0; i--)
            {
                _chatfield[t].text = _chat.messages[i].sender + ": " + _chat.messages[i].message;
                t++;
                if (t > 8)
                {
                    break;
                }
            }
        }
        else
        {
            Debug.Log("chat tyhmä");
        }
     
    }
    public void SubmitInputField()
    {
        string message = inputText.text;
        inputText.text = "";
        NewChatMessage newChatMessage = new NewChatMessage();
        newChatMessage.message = message;
        newChatMessage.sender = _playerName;
        PostData(newChatMessage);
    }
    public void ChangeChannel()
    {
        string tempChannel = inputChannel.text;
        if(tempChannel == "")
        {
            tempChannel = "lobby";
        }
        inputChannel.text = "";
        _url = _baseurl + tempChannel;

       string json = JsonUtility.ToJson("");
        Hashtable headers = new Hashtable();
        headers.Add("Content-Type", "application/json");
        byte[] body = Encoding.UTF8.GetBytes(json);
        WWW www = new WWW(_url);
        StartCoroutine("GetDataEnumerator", www);
        ToggleChannel();

       
    }
    public void RefreshChat()
    {
        WWW www = new WWW(_url);
        StartCoroutine("GetDataEnumerator", www);
    }
    void PostData(NewChatMessage message)
    {
        if (message.message != null&& message.message.Length !=0)
        {
            string json = JsonUtility.ToJson(message);
            Hashtable headers = new Hashtable();
            headers.Add("Content-Type", "application/json");
            byte[] body = Encoding.UTF8.GetBytes(json);
            WWW www = new WWW(_url, body, headers);
            StartCoroutine("PostdataEnumerator", www);
        }
        else
        {
            Debug.Log("Virheellinen viesti");
        }
    }
    IEnumerator GetDataEnumerator(WWW www)
    {
        yield return www;
        if (www.error != null)
        {
            Debug.Log("Ei toimi");
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.text);
            Chat response = new Chat();
            JsonUtility.FromJsonOverwrite(www.text, response);
            Debug.Log(response);
            _chat = response;
            RenderChatToScreen();

        }
    }
    IEnumerator PostdataEnumerator(WWW www)
    {
        yield return www;
        if (www.error != null)
        {
            Debug.Log("Ei toimi");
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.text);
            Chat response = new Chat();
            JsonUtility.FromJsonOverwrite(www.text, response);
            Debug.Log(response);
            _chat = response;
            RenderChatToScreen();

        }
    }
}
[Serializable]
public class NewChatMessage
{
    public string message;
    public string sender;
}
[Serializable]
public class ChatMessage
{
    public string message;
    public string sender;
    public long created;
}
[Serializable]
public class Chat
{
    public ChatMessage[] messages;
   
}